package main

import (
    "fmt"
    "github.com/hyperledger/fabric/core/chaincode/shim"
    "testing"
)

//func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
//    res := stub.MockInit("1", args)
//    if res.Status != shim.OK {
//        fmt.Println("Init failed", string(res.Message))
//        t.FailNow()
//    }
//}
//
func checkQuery(t *testing.T, stub *shim.MockStub, id string) {
   res := stub.MockInvoke("1", [][]byte{[]byte("query"), []byte(id)})
   if res.Status != shim.OK {
       fmt.Println("Query", id, "failed", string(res.Message))
       t.FailNow()
   }
   if res.Payload == nil {
       fmt.Println("Query", id, "failed to get value")
       t.FailNow()
   }

   fmt.Println("Query value", id, "was ", string(res.Payload))

}

func checkQueryAll(t *testing.T, stub *shim.MockStub, id string) {
   res := stub.MockInvoke("1", [][]byte{[]byte("queryAll"), []byte(id)})
   if res.Status != shim.OK {
       fmt.Println("1Query", id, "failed", res.Message)
       t.FailNow()
   }
   if res.Payload == nil {
       fmt.Println("2Query", id, "failed to get value")
       t.FailNow()
   }

   fmt.Println("3Query value", id, "was ", string(res.Payload))

}
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
    res := stub.MockInvoke("1", args)
    if res.Status != shim.OK {
        fmt.Println("Invoke", args, "failed", string(res.Message))
        t.FailNow()
    }
}
func Test_Rk(t *testing.T) {

    rk := new(rk)
    stub := shim.NewMockStub("rk", rk)

    //checkInit(t, stub, [][]byte{[]byte("str"), []byte("helloworld")})
    //checkQuery(t, stub, "str")
//  []byte("{\"DocType\":\"rktype\",\"Id\":\"1\",\"Rkdh\":\"110114\",\"Rktype\":\"1\",\"Rksl\":\"18910012222\",\"Rkfs\":\"18910012222\",\"Rkzt\":\"18910012222\" ,\"Rkczry\":\"18910012222\",\"Rktg\":\"18910012222\",\"Rktime\":\"18910012222\",\"Rkbz\":\"18910012222\"}")}
   checkInvoke(t, stub, [][]byte{[]byte("save"), []byte("1"), []byte("{\"DocType\":\"110114\",\"rktype\":\"110114\",\"Id\":\"1\",\"Rkdh\":\"110114\",\"Rktype\":\"1\",\"Rksl\":\"18910012222\",\"Rkfs\":\"18910012222\",\"Rkzt\":\"18910012222\" ,\"Rkczry\":\"18910012222\",\"Rktg\":\"18910012222\",\"Rktime\":\"18910012222\",\"Rkbz\":\"18910012222\"}")})
    checkInvoke(t, stub, [][]byte{[]byte("save"), []byte("2"), []byte("{\"DocType\":\"110\",\"rktype\":\"11\",\"Id\":\"1\",\"Rkdh\":\"110114\",\"Rktype\":\"1\",\"Rksl\":\"18910012222\",\"Rkfs\":\"18910012222\",\"Rkzt\":\"18910012222\" ,\"Rkczry\":\"18910012222\",\"Rktg\":\"18910012222\",\"Rktime\":\"18910012222\",\"Rkbz\":\"18910012222\"}")})
    checkInvoke(t, stub, [][]byte{[]byte("delete"), []byte("2")})
    checkQuery(t, stub, "1")
    checkQuery(t, stub, "2")
}
