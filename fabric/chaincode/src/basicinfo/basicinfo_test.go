package main

import (
    "fmt"
    "testing"
    "github.com/hyperledger/fabric/core/chaincode/shim"
)

//func checkInit(t *testing.T, stub *shim.MockStub, args [][]byte) {
//    res := stub.MockInit("1", args)
//    if res.Status != shim.OK {
//        fmt.Println("Init failed", string(res.Message))
//        t.FailNow()
//    }
//}
//
func checkQuery(t *testing.T, stub *shim.MockStub, name string) {
   res := stub.MockInvoke("1", [][]byte{[]byte("query"), []byte(name)})
   if res.Status != shim.OK {
       fmt.Println("Query", name, "failed", string(res.Message))
       t.FailNow()
   }
   if res.Payload == nil {
       fmt.Println("Query", name, "failed to get value")
       t.FailNow()
   }

   fmt.Println("Query value", name, "was ", string(res.Payload))

}
func checkInvoke(t *testing.T, stub *shim.MockStub, args [][]byte) {
    res := stub.MockInvoke("1", args)
    if res.Status != shim.OK {
        fmt.Println("Invoke", args, "failed", string(res.Message))
        t.FailNow()
    }
}
func Test_BasicInfo(t *testing.T) {

    hello := new(basicInfo)
    stub := shim.NewMockStub("hello", hello)

    //checkInit(t, stub, [][]byte{[]byte("str"), []byte("helloworld")})
    //checkQuery(t, stub, "str")

    checkInvoke(t, stub, [][]byte{[]byte("save"), []byte("110114"), []byte("{\"name\":\"zhangsan\",\"identity\":\"110114\",\"mobile\":\"18910012222\"}")})
    checkQuery(t, stub, "110114")
}
