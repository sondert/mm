import request from '@/utils/request'

// 查询物资资料列表
export function listWzwz(query) {
  return request({
    url: '/wzwz/wzwz/list',
    method: 'get',
    params: query
  })
}

// 查询物资资料详细
export function getWzwz(id) {
  return request({
    url: '/wzwz/wzwz/' + id,
    method: 'get'
  })
}

// 新增物资资料
export function addWzwz(data) {
  return request({
    url: '/wzwz/wzwz',
    method: 'post',
    data: data
  })
}

// 修改物资资料
export function updateWzwz(data) {
  return request({
    url: '/wzwz/wzwz',
    method: 'put',
    data: data
  })
}

// 批准
export function pz(id) {
  return request({
    url: '/wzwz/wzwz/pz/'+id,
    method: 'put'
  })
}

// 删除物资资料
export function delWzwz(id) {
  return request({
    url: '/wzwz/wzwz/' + id,
    method: 'delete'
  })
}

// 导出物资资料
export function exportWzwz(query) {
  return request({
    url: '/wzwz/wzwz/export',
    method: 'get',
    params: query
  })
}
