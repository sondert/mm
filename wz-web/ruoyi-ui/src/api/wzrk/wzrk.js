import request from '@/utils/request'

// 查询物资入库表列表
export function listWzrk(query) {
  return request({
    url: '/wzrk/wzrk/list',
    method: 'get',
    params: query
  })
}

// 查询物资入库表详细
export function getWzrk(id) {
  return request({
    url: '/wzrk/wzrk/' + id,
    method: 'get'
  })
}

// 新增物资入库表
export function addWzrk(data,ids,nums) {
  return request({
    url: '/wzrk/wzrk/'+ids+'/'+nums,
    method: 'post',
    data: data
  })
}

// 修改物资入库表
export function updateWzrk(data) {
  return request({
    url: '/wzrk/wzrk',
    method: 'put',
    data: data
  })
}

// 删除物资入库表
export function delWzrk(id) {
  return request({
    url: '/wzrk/wzrk/' + id,
    method: 'delete'
  })
}

// 导出物资入库表
export function exportWzrk(query) {
  return request({
    url: '/wzrk/wzrk/export',
    method: 'get',
    params: query
  })
}

// 物资溯源
export function trace(id) {
  return request({
    url: '/wzrk/wzrk/trace/'+id,
    method: 'get'
  })
}

// 物资批准入库
export function rk(id) {
  return request({
    url: '/wzrk/wzrk/rk/'+id,
    method: 'put'
  })
}

// 比对
export function bd() {
  return request({
    url: '/wzrk/wzrk/bd/',
    method: 'put'
  })
}

// 物资
export function wz() {
  return request({
    url: '/wzrk/wzrk/wz/',
    method: 'get'
  })
}

// 明细
export function mx(id) {
  return request({
    url: '/wzrk/wzrk/mx/'+id,
    method: 'get'
  })
}
