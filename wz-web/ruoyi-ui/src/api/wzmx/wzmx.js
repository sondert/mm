import request from '@/utils/request'

// 查询物资明细列表
export function listWzmx(query) {
  return request({
    url: '/wzmx/wzmx/list',
    method: 'get',
    params: query
  })
}

// 查询物资明细详细
export function getWzmx(id) {
  return request({
    url: '/wzmx/wzmx/' + id,
    method: 'get'
  })
}

// 新增物资明细
export function addWzmx(data) {
  return request({
    url: '/wzmx/wzmx',
    method: 'post',
    data: data
  })
}

// 修改物资明细
export function updateWzmx(data) {
  return request({
    url: '/wzmx/wzmx',
    method: 'put',
    data: data
  })
}

// 删除物资明细
export function delWzmx(id) {
  return request({
    url: '/wzmx/wzmx/' + id,
    method: 'delete'
  })
}

// 导出物资明细
export function exportWzmx(query) {
  return request({
    url: '/wzmx/wzmx/export',
    method: 'get',
    params: query
  })
}