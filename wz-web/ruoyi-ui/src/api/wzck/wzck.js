import request from '@/utils/request'

// 查询仓库信息列表
export function listWzck(query) {
  return request({
    url: '/wzck/wzck/list',
    method: 'get',
    params: query
  })
}

// 查询仓库信息详细
export function getWzck(id) {
  return request({
    url: '/wzck/wzck/' + id,
    method: 'get'
  })
}

// 新增仓库信息
export function addWzck(data) {
  return request({
    url: '/wzck/wzck',
    method: 'post',
    data: data
  })
}

// 修改仓库信息
export function updateWzck(data) {
  return request({
    url: '/wzck/wzck',
    method: 'put',
    data: data
  })
}

// 删除仓库信息
export function delWzck(id) {
  return request({
    url: '/wzck/wzck/' + id,
    method: 'delete'
  })
}

// 导出仓库信息
export function exportWzck(query) {
  return request({
    url: '/wzck/wzck/export',
    method: 'get',
    params: query
  })
}