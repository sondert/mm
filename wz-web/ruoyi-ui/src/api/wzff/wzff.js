import request from '@/utils/request'

// 查询物资发放列表
export function listWzff(query) {
  return request({
    url: '/wzff/wzff/list',
    method: 'get',
    params: query
  })
}

// 查询物资发放详细
export function getWzff(id) {
  return request({
    url: '/wzff/wzff/' + id,
    method: 'get'
  })
}

// 新增物资发放
export function addWzff(data,ids,nums) {
  return request({
    url: '/wzff/wzff/'+ids+'/'+nums,
    method: 'post',
    data: data
  })
}

// 修改物资发放
export function updateWzff(data) {
  return request({
    url: '/wzff/wzff',
    method: 'put',
    data: data
  })
}

// 删除物资发放
export function delWzff(id) {
  return request({
    url: '/wzff/wzff/' + id,
    method: 'delete'
  })
}

// 导出物资发放
export function exportWzff(query) {
  return request({
    url: '/wzff/wzff/export',
    method: 'get',
    params: query
  })
}

// 物资溯源
export function trace(id) {
  return request({
    url: '/wzff/wzff/trace/'+id,
    method: 'get'
  })
}

// 物资批准发放
export function ff(id) {
  return request({
    url: '/wzff/wzff/ff/'+id,
    method: 'put'
  })
}

// 比对
export function bd() {
  return request({
    url: '/wzff/wzff/bd/',
    method: 'put'
  })
}

// 库存
export function kc(id) {
  return request({
    url: '/wzff/wzff/kc/'+id,
    method: 'get'
  })
}

// 明细
export function mx(id) {
  return request({
    url: '/wzff/wzff/mx/'+id,
    method: 'get'
  })
}
