import request from '@/utils/request'

// 查询车辆信息表列表
export function listWzcl(query) {
  return request({
    url: '/wzcl/wzcl/list',
    method: 'get',
    params: query
  })
}

// 查询车辆信息表详细
export function getWzcl(id) {
  return request({
    url: '/wzcl/wzcl/' + id,
    method: 'get'
  })
}

// 新增车辆信息表
export function addWzcl(data) {
  return request({
    url: '/wzcl/wzcl',
    method: 'post',
    data: data
  })
}

// 修改车辆信息表
export function updateWzcl(data) {
  return request({
    url: '/wzcl/wzcl',
    method: 'put',
    data: data
  })
}

// 删除车辆信息表
export function delWzcl(id) {
  return request({
    url: '/wzcl/wzcl/' + id,
    method: 'delete'
  })
}

// 导出车辆信息表
export function exportWzcl(query) {
  return request({
    url: '/wzcl/wzcl/export',
    method: 'get',
    params: query
  })
}