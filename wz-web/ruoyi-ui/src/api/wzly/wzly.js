import request from '@/utils/request'

// 查询物资来源列表
export function listWzly(query) {
  return request({
    url: '/wzly/wzly/list',
    method: 'get',
    params: query
  })
}

// 查询物资来源详细
export function getWzly(id) {
  return request({
    url: '/wzly/wzly/' + id,
    method: 'get'
  })
}

// 新增物资来源
export function addWzly(data) {
  return request({
    url: '/wzly/wzly',
    method: 'post',
    data: data
  })
}

// 修改物资来源
export function updateWzly(data) {
  return request({
    url: '/wzly/wzly',
    method: 'put',
    data: data
  })
}

// 删除物资来源
export function delWzly(id) {
  return request({
    url: '/wzly/wzly/' + id,
    method: 'delete'
  })
}

// 导出物资来源
export function exportWzly(query) {
  return request({
    url: '/wzly/wzly/export',
    method: 'get',
    params: query
  })
}