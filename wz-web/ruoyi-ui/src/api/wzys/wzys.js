import request from '@/utils/request'

// 查询物资运输列表
export function listWzys(query) {
  return request({
    url: '/wzys/wzys/list',
    method: 'get',
    params: query
  })
}

// 查询物资运输详细
export function getWzys(id) {
  return request({
    url: '/wzys/wzys/' + id,
    method: 'get'
  })
}

// 新增物资运输
export function addWzys(data,ids,nums) {
  return request({
    url: '/wzys/wzys/'+ids+'/'+nums,
    method: 'post',
    data: data
  })
}

// 修改物资运输
export function updateWzys(data) {
  return request({
    url: '/wzys/wzys',
    method: 'put',
    data: data
  })
}

// 删除物资运输
export function delWzys(id) {
  return request({
    url: '/wzys/wzys/' + id,
    method: 'delete'
  })
}

// 导出物资运输
export function exportWzys(query) {
  return request({
    url: '/wzys/wzys/export',
    method: 'get',
    params: query
  })
}

// 物资溯源
export function trace(id) {
  return request({
    url: '/wzys/wzys/trace/'+id,
    method: 'get'
  })
}

// 物资批准运输
export function ys(id) {
  return request({
    url: '/wzys/wzys/ys/'+id,
    method: 'put'
  })
}

// 物资批准运输
export function sd(id) {
  return request({
    url: '/wzys/wzys/sd/'+id,
    method: 'put'
  })
}

// 比对
export function bd() {
  return request({
    url: '/wzys/wzys/bd/',
    method: 'put'
  })
}

// 库存
export function kc(id) {
  return request({
    url: '/wzys/wzys/kc/'+id,
    method: 'get'
  })
}

// 明细
export function mx(id) {
  return request({
    url: '/wzys/wzys/mx/'+id,
    method: 'get'
  })
}

// 修改位置
export function wz(id,wz) {
  return request({
    url: '/wzys/wzys/wz/'+id+'/'+wz,
    method: 'post'
  })
}


