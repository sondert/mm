import request from '@/utils/request'

// 查询物资日志列表
export function listWzrz(query) {
  return request({
    url: '/wzrz/wzrz/list',
    method: 'get',
    params: query
  })
}

// 查询物资日志详细
export function getWzrz(id) {
  return request({
    url: '/wzrz/wzrz/' + id,
    method: 'get'
  })
}

// 新增物资日志
export function addWzrz(data) {
  return request({
    url: '/wzrz/wzrz',
    method: 'post',
    data: data
  })
}

// 修改物资日志
export function updateWzrz(data) {
  return request({
    url: '/wzrz/wzrz',
    method: 'put',
    data: data
  })
}

// 删除物资日志
export function delWzrz(id) {
  return request({
    url: '/wzrz/wzrz/' + id,
    method: 'delete'
  })
}

// 导出物资日志
export function exportWzrz(query) {
  return request({
    url: '/wzrz/wzrz/export',
    method: 'get',
    params: query
  })
}