import request from '@/utils/request'

// 查询物资流向列表
export function listWzqc(query) {
  return request({
    url: '/wzqc/wzqc/list',
    method: 'get',
    params: query
  })
}

// 查询物资流向详细
export function getWzqc(id) {
  return request({
    url: '/wzqc/wzqc/' + id,
    method: 'get'
  })
}

// 新增物资流向
export function addWzqc(data) {
  return request({
    url: '/wzqc/wzqc',
    method: 'post',
    data: data
  })
}

// 修改物资流向
export function updateWzqc(data) {
  return request({
    url: '/wzqc/wzqc',
    method: 'put',
    data: data
  })
}

// 删除物资流向
export function delWzqc(id) {
  return request({
    url: '/wzqc/wzqc/' + id,
    method: 'delete'
  })
}

// 导出物资流向
export function exportWzqc(query) {
  return request({
    url: '/wzqc/wzqc/export',
    method: 'get',
    params: query
  })
}