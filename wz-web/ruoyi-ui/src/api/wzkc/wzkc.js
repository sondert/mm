import request from '@/utils/request'

// 查询物资库存列表
export function listWzkc(query) {
  return request({
    url: '/wzkc/wzkc/list',
    method: 'get',
    params: query
  })
}

// 查询物资库存详细
export function getWzkc(id) {
  return request({
    url: '/wzkc/wzkc/' + id,
    method: 'get'
  })
}

// 新增物资库存
export function addWzkc(data) {
  return request({
    url: '/wzkc/wzkc',
    method: 'post',
    data: data
  })
}

// 修改物资库存
export function updateWzkc(data) {
  return request({
    url: '/wzkc/wzkc',
    method: 'put',
    data: data
  })
}

// 删除物资库存
export function delWzkc(id) {
  return request({
    url: '/wzkc/wzkc/' + id,
    method: 'delete'
  })
}

// 导出物资库存
export function exportWzkc(query) {
  return request({
    url: '/wzkc/wzkc/export',
    method: 'get',
    params: query
  })
}

// 导出物资库存
export function zs() {
  return request({
    url: '/wzkc/wzkc/zs',
    method: 'get'
  })
}
