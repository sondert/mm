package com.ruoyi.wzmx.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物资明细对象 wz_mx
 * 
 * @author sonder
 * @date 2021-11-16
 */
public class WzMx extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 明细id */
    private Integer id;

    /** 单号 */
    @Excel(name = "单号")
    private String mxDh;

    /** 物资名称 */
    @Excel(name = "物资名称")
    private String mxMc;

    /** 商品id */
    @Excel(name = "商品id")
    private Integer mxSpid;

    /** 规格 */
    @Excel(name = "规格")
    private String mxGg;

    /** 图片 */
    @Excel(name = "图片")
    private String mxTp;

    /** 数量 */
    @Excel(name = "数量")
    private Integer mxNum;

    /** 单位 */
    @Excel(name = "单位")
    private String mxDw;

    /** 类型 */
    @Excel(name = "类型")
    private Integer mxType;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setMxDh(String mxDh) 
    {
        this.mxDh = mxDh;
    }

    public String getMxDh() 
    {
        return mxDh;
    }
    public void setMxMc(String mxMc) 
    {
        this.mxMc = mxMc;
    }

    public String getMxMc() 
    {
        return mxMc;
    }
    public void setMxSpid(Integer mxSpid) 
    {
        this.mxSpid = mxSpid;
    }

    public Integer getMxSpid() 
    {
        return mxSpid;
    }
    public void setMxGg(String mxGg) 
    {
        this.mxGg = mxGg;
    }

    public String getMxGg() 
    {
        return mxGg;
    }
    public void setMxTp(String mxTp) 
    {
        this.mxTp = mxTp;
    }

    public String getMxTp() 
    {
        return mxTp;
    }
    public void setMxNum(Integer mxNum) 
    {
        this.mxNum = mxNum;
    }

    public Integer getMxNum() 
    {
        return mxNum;
    }
    public void setMxDw(String mxDw) 
    {
        this.mxDw = mxDw;
    }

    public String getMxDw() 
    {
        return mxDw;
    }
    public void setMxType(Integer mxType) 
    {
        this.mxType = mxType;
    }

    public Integer getMxType() 
    {
        return mxType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("mxDh", getMxDh())
            .append("mxMc", getMxMc())
            .append("mxSpid", getMxSpid())
            .append("mxGg", getMxGg())
            .append("mxTp", getMxTp())
            .append("mxNum", getMxNum())
            .append("mxDw", getMxDw())
            .append("mxType", getMxType())
            .toString();
    }
}
