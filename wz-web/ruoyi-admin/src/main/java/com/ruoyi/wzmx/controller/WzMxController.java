package com.ruoyi.wzmx.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wzmx.domain.WzMx;
import com.ruoyi.wzmx.service.IWzMxService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物资明细Controller
 * 
 * @author sonder
 * @date 2021-11-16
 */
@RestController
@RequestMapping("/wzmx/wzmx")
public class WzMxController extends BaseController
{
    @Autowired
    private IWzMxService wzMxService;

    /**
     * 查询物资明细列表
     */
    @PreAuthorize("@ss.hasPermi('wzmx:wzmx:list')")
    @GetMapping("/list")
    public TableDataInfo list(WzMx wzMx)
    {
        startPage();
        List<WzMx> list = wzMxService.selectWzMxList(wzMx);
        return getDataTable(list);
    }

    /**
     * 导出物资明细列表
     */
    @PreAuthorize("@ss.hasPermi('wzmx:wzmx:export')")
    @Log(title = "物资明细", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WzMx wzMx)
    {
        List<WzMx> list = wzMxService.selectWzMxList(wzMx);
        ExcelUtil<WzMx> util = new ExcelUtil<WzMx>(WzMx.class);
        return util.exportExcel(list, "物资明细数据");
    }

    /**
     * 获取物资明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('wzmx:wzmx:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(wzMxService.selectWzMxById(id));
    }

    /**
     * 新增物资明细
     */
    @PreAuthorize("@ss.hasPermi('wzmx:wzmx:add')")
    @Log(title = "物资明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WzMx wzMx)
    {
        return toAjax(wzMxService.insertWzMx(wzMx));
    }

    /**
     * 修改物资明细
     */
    @PreAuthorize("@ss.hasPermi('wzmx:wzmx:edit')")
    @Log(title = "物资明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WzMx wzMx)
    {
        return toAjax(wzMxService.updateWzMx(wzMx));
    }

    /**
     * 删除物资明细
     */
    @PreAuthorize("@ss.hasPermi('wzmx:wzmx:remove')")
    @Log(title = "物资明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(wzMxService.deleteWzMxByIds(ids));
    }
}
