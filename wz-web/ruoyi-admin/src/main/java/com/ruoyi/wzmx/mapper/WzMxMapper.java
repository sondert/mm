package com.ruoyi.wzmx.mapper;

import java.util.List;
import com.ruoyi.wzmx.domain.WzMx;

/**
 * 物资明细Mapper接口
 * 
 * @author sonder
 * @date 2021-11-16
 */
public interface WzMxMapper 
{
    /**
     * 查询物资明细
     * 
     * @param id 物资明细主键
     * @return 物资明细
     */
    public WzMx selectWzMxById(Integer id);

    /**
     * 查询物资明细列表
     * 
     * @param wzMx 物资明细
     * @return 物资明细集合
     */
    public List<WzMx> selectWzMxList(WzMx wzMx);

    /**
     * 新增物资明细
     * 
     * @param wzMx 物资明细
     * @return 结果
     */
    public int insertWzMx(WzMx wzMx);

    /**
     * 修改物资明细
     * 
     * @param wzMx 物资明细
     * @return 结果
     */
    public int updateWzMx(WzMx wzMx);

    /**
     * 删除物资明细
     * 
     * @param id 物资明细主键
     * @return 结果
     */
    public int deleteWzMxById(Integer id);

    /**
     * 批量删除物资明细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWzMxByIds(Integer[] ids);
}
