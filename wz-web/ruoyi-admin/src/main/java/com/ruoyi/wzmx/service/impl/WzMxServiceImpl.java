package com.ruoyi.wzmx.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wzmx.mapper.WzMxMapper;
import com.ruoyi.wzmx.domain.WzMx;
import com.ruoyi.wzmx.service.IWzMxService;

/**
 * 物资明细Service业务层处理
 * 
 * @author sonder
 * @date 2021-11-16
 */
@Service
public class WzMxServiceImpl implements IWzMxService 
{
    @Autowired
    private WzMxMapper wzMxMapper;

    /**
     * 查询物资明细
     * 
     * @param id 物资明细主键
     * @return 物资明细
     */
    @Override
    public WzMx selectWzMxById(Integer id)
    {
        return wzMxMapper.selectWzMxById(id);
    }

    /**
     * 查询物资明细列表
     * 
     * @param wzMx 物资明细
     * @return 物资明细
     */
    @Override
    public List<WzMx> selectWzMxList(WzMx wzMx)
    {
        return wzMxMapper.selectWzMxList(wzMx);
    }

    /**
     * 新增物资明细
     * 
     * @param wzMx 物资明细
     * @return 结果
     */
    @Override
    public int insertWzMx(WzMx wzMx)
    {
        return wzMxMapper.insertWzMx(wzMx);
    }

    /**
     * 修改物资明细
     * 
     * @param wzMx 物资明细
     * @return 结果
     */
    @Override
    public int updateWzMx(WzMx wzMx)
    {
        return wzMxMapper.updateWzMx(wzMx);
    }

    /**
     * 批量删除物资明细
     * 
     * @param ids 需要删除的物资明细主键
     * @return 结果
     */
    @Override
    public int deleteWzMxByIds(Integer[] ids)
    {
        return wzMxMapper.deleteWzMxByIds(ids);
    }

    /**
     * 删除物资明细信息
     * 
     * @param id 物资明细主键
     * @return 结果
     */
    @Override
    public int deleteWzMxById(Integer id)
    {
        return wzMxMapper.deleteWzMxById(id);
    }
}
