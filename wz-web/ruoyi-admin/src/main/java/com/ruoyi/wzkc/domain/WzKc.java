package com.ruoyi.wzkc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物资库存对象 wz_kc
 * 
 * @author sonder
 * @date 2021-11-14
 */
public class WzKc extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 库存id */
    private Integer id;

    /** 仓库id */
    @Excel(name = "仓库id")
    private Integer ckCkid;

    /** 物资id */
    @Excel(name = "物资id")
    private Integer kcWzid;

    /** 数量 */
    @Excel(name = "数量")
    private Integer num;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setCkCkid(Integer ckCkid) 
    {
        this.ckCkid = ckCkid;
    }

    public Integer getCkCkid() 
    {
        return ckCkid;
    }
    public void setKcWzid(Integer kcWzid) 
    {
        this.kcWzid = kcWzid;
    }

    public Integer getKcWzid() 
    {
        return kcWzid;
    }
    public void setNum(Integer num) 
    {
        this.num = num;
    }

    public Integer getNum() 
    {
        return num;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ckCkid", getCkCkid())
            .append("kcWzid", getKcWzid())
            .append("num", getNum())
            .toString();
    }
}
