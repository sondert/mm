package com.ruoyi.wzkc.service;

import java.util.List;
import com.ruoyi.wzkc.domain.WzKc;

/**
 * 物资库存Service接口
 * 
 * @author sonder
 * @date 2021-11-14
 */
public interface IWzKcService 
{
    /**
     * 查询物资库存
     * 
     * @param id 物资库存主键
     * @return 物资库存
     */
    public WzKc selectWzKcById(Integer id);

    /**
     * 查询物资库存列表
     * 
     * @param wzKc 物资库存
     * @return 物资库存集合
     */
    public List<WzKc> selectWzKcList(WzKc wzKc);

    /**
     * 新增物资库存
     * 
     * @param wzKc 物资库存
     * @return 结果
     */
    public int insertWzKc(WzKc wzKc);

    /**
     * 修改物资库存
     * 
     * @param wzKc 物资库存
     * @return 结果
     */
    public int updateWzKc(WzKc wzKc);

    /**
     * 批量删除物资库存
     * 
     * @param ids 需要删除的物资库存主键集合
     * @return 结果
     */
    public int deleteWzKcByIds(Integer[] ids);

    /**
     * 删除物资库存信息
     * 
     * @param id 物资库存主键
     * @return 结果
     */
    public int deleteWzKcById(Integer id);
}
