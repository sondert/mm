package com.ruoyi.wzkc.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wzkc.mapper.WzKcMapper;
import com.ruoyi.wzkc.domain.WzKc;
import com.ruoyi.wzkc.service.IWzKcService;

/**
 * 物资库存Service业务层处理
 * 
 * @author sonder
 * @date 2021-11-14
 */
@Service
public class WzKcServiceImpl implements IWzKcService 
{
    @Autowired
    private WzKcMapper wzKcMapper;

    /**
     * 查询物资库存
     * 
     * @param id 物资库存主键
     * @return 物资库存
     */
    @Override
    public WzKc selectWzKcById(Integer id)
    {
        return wzKcMapper.selectWzKcById(id);
    }

    /**
     * 查询物资库存列表
     * 
     * @param wzKc 物资库存
     * @return 物资库存
     */
    @Override
    public List<WzKc> selectWzKcList(WzKc wzKc)
    {
        return wzKcMapper.selectWzKcList(wzKc);
    }

    /**
     * 新增物资库存
     * 
     * @param wzKc 物资库存
     * @return 结果
     */
    @Override
    public int insertWzKc(WzKc wzKc)
    {
        return wzKcMapper.insertWzKc(wzKc);
    }

    /**
     * 修改物资库存
     * 
     * @param wzKc 物资库存
     * @return 结果
     */
    @Override
    public int updateWzKc(WzKc wzKc)
    {
        return wzKcMapper.updateWzKc(wzKc);
    }

    /**
     * 批量删除物资库存
     * 
     * @param ids 需要删除的物资库存主键
     * @return 结果
     */
    @Override
    public int deleteWzKcByIds(Integer[] ids)
    {
        return wzKcMapper.deleteWzKcByIds(ids);
    }

    /**
     * 删除物资库存信息
     * 
     * @param id 物资库存主键
     * @return 结果
     */
    @Override
    public int deleteWzKcById(Integer id)
    {
        return wzKcMapper.deleteWzKcById(id);
    }
}
