package com.ruoyi.wzkc.controller;

import java.util.LinkedList;
import java.util.List;

import com.ruoyi.wzck.domain.WzCk;
import com.ruoyi.wzck.service.IWzCkService;
import com.ruoyi.wzwz.domain.WzWz;
import com.ruoyi.wzwz.service.IWzWzService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wzkc.domain.WzKc;
import com.ruoyi.wzkc.service.IWzKcService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物资库存Controller
 * 
 * @author sonder
 * @date 2021-11-14
 */
@RestController
@RequestMapping("/wzkc/wzkc")
public class WzKcController extends BaseController
{
    @Autowired
    private IWzKcService wzKcService;

    @Autowired
    private IWzCkService wzCkService;

    @Autowired
    private IWzWzService wzWzService;
    /**
     * 查询物资库存列表
     */
    @PreAuthorize("@ss.hasPermi('wzkc:wzkc:list')")
    @GetMapping("/list")
    public TableDataInfo list(WzKc wzKc)
    {
        startPage();
        List<WzKc> list = wzKcService.selectWzKcList(wzKc);
        return getDataTable(list);
    }

    /**
     * 导出物资库存列表
     */
    @PreAuthorize("@ss.hasPermi('wzkc:wzkc:export')")
    @Log(title = "物资库存", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WzKc wzKc)
    {
        List<WzKc> list = wzKcService.selectWzKcList(wzKc);
        ExcelUtil<WzKc> util = new ExcelUtil<WzKc>(WzKc.class);
        return util.exportExcel(list, "物资库存数据");
    }

    /**
     * 获取物资库存详细信息
     */
    @PreAuthorize("@ss.hasPermi('wzkc:wzkc:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(wzKcService.selectWzKcById(id));
    }

    /**
     * 新增物资库存
     */
    @PreAuthorize("@ss.hasPermi('wzkc:wzkc:add')")
    @Log(title = "物资库存", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WzKc wzKc)
    {
        return toAjax(wzKcService.insertWzKc(wzKc));
    }

    /**
     * 修改物资库存
     */
    @PreAuthorize("@ss.hasPermi('wzkc:wzkc:edit')")
    @Log(title = "物资库存", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WzKc wzKc)
    {
        return toAjax(wzKcService.updateWzKc(wzKc));
    }

    /**
     * 删除物资库存
     */
    @PreAuthorize("@ss.hasPermi('wzkc:wzkc:remove')")
    @Log(title = "物资库存", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(wzKcService.deleteWzKcByIds(ids));
    }

    @PreAuthorize("@ss.hasPermi('wzkc:wzkc:zs')")
    @GetMapping(value = "/zs")
    public TableDataInfo zs()
    {
        List<WzCk> list = wzCkService.selectWzCkList(new WzCk());
        List<WzWz> wzlist = wzWzService.selectWzWzList(new WzWz());
        int wzn = wzlist.size();

        List<String[]> ans = new LinkedList<>();
        int ckn=list.size();
        String[][] sz = new String[wzn+1][ckn+1];
        int i=1;
        sz[0][0]="库存";
        for(WzCk ck:list){
            sz[0][i++]=ck.getId()+"";
        }
        int j=1;
        for(WzWz wzWz:wzlist){
            sz[j++][0]=wzWz.getId()+"";
        }
        for(int a=1;a<=wzn;a++){
            for(int b=1;b<=ckn;b++){
                WzKc wzKc = new WzKc();
                wzKc.setCkCkid(Integer.parseInt(sz[0][b]));
                wzKc.setKcWzid(Integer.parseInt(sz[a][0]));
                List<WzKc> kcList = wzKcService.selectWzKcList(wzKc);
                if(kcList.size()==1){
                    sz[a][b]=kcList.get(0).getNum()+"";
                }else if(kcList.size()==0){
                    sz[a][b] = "0";
                }else {
                    System.out.println("出错");
                }
            }
        }
        for(int a=1;a<sz[0].length;a++){
            sz[0][a] = wzCkService.selectWzCkById(Integer.parseInt(sz[0][a])).getCkName();
        }
        for(int a=1;a<sz.length;a++){
            sz[a][0] = wzWzService.selectWzWzById(Integer.parseInt(sz[a][0])).getWzName();
        }
        for(int a=0;a<sz.length;a++){
            ans.add(sz[a]);
        }
        return getDataTable(ans);
    }
}

