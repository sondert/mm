package com.ruoyi.wzck.mapper;

import java.util.List;
import com.ruoyi.wzck.domain.WzCk;

/**
 * 仓库信息Mapper接口
 * 
 * @author sonder
 * @date 2021-11-14
 */
public interface WzCkMapper 
{
    /**
     * 查询仓库信息
     * 
     * @param id 仓库信息主键
     * @return 仓库信息
     */
    public WzCk selectWzCkById(Integer id);

    /**
     * 查询仓库信息列表
     * 
     * @param wzCk 仓库信息
     * @return 仓库信息集合
     */
    public List<WzCk> selectWzCkList(WzCk wzCk);

    /**
     * 新增仓库信息
     * 
     * @param wzCk 仓库信息
     * @return 结果
     */
    public int insertWzCk(WzCk wzCk);

    /**
     * 修改仓库信息
     * 
     * @param wzCk 仓库信息
     * @return 结果
     */
    public int updateWzCk(WzCk wzCk);

    /**
     * 删除仓库信息
     * 
     * @param id 仓库信息主键
     * @return 结果
     */
    public int deleteWzCkById(Integer id);

    /**
     * 批量删除仓库信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWzCkByIds(Integer[] ids);
}
