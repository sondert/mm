package com.ruoyi.wzck.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wzck.domain.WzCk;
import com.ruoyi.wzck.service.IWzCkService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 仓库信息Controller
 * 
 * @author sonder
 * @date 2021-11-14
 */
@RestController
@RequestMapping("/wzck/wzck")
public class WzCkController extends BaseController
{
    @Autowired
    private IWzCkService wzCkService;

    /**
     * 查询仓库信息列表
     */
    @PreAuthorize("@ss.hasPermi('wzck:wzck:list')")
    @GetMapping("/list")
    public TableDataInfo list(WzCk wzCk)
    {
        startPage();
        List<WzCk> list = wzCkService.selectWzCkList(wzCk);
        return getDataTable(list);
    }

    /**
     * 导出仓库信息列表
     */
    @PreAuthorize("@ss.hasPermi('wzck:wzck:export')")
    @Log(title = "仓库信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WzCk wzCk)
    {
        List<WzCk> list = wzCkService.selectWzCkList(wzCk);
        ExcelUtil<WzCk> util = new ExcelUtil<WzCk>(WzCk.class);
        return util.exportExcel(list, "仓库信息数据");
    }

    /**
     * 获取仓库信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('wzck:wzck:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(wzCkService.selectWzCkById(id));
    }

    /**
     * 新增仓库信息
     */
    @PreAuthorize("@ss.hasPermi('wzck:wzck:add')")
    @Log(title = "仓库信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WzCk wzCk)
    {
        return toAjax(wzCkService.insertWzCk(wzCk));
    }

    /**
     * 修改仓库信息
     */
    @PreAuthorize("@ss.hasPermi('wzck:wzck:edit')")
    @Log(title = "仓库信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WzCk wzCk)
    {
        return toAjax(wzCkService.updateWzCk(wzCk));
    }

    /**
     * 删除仓库信息
     */
    @PreAuthorize("@ss.hasPermi('wzck:wzck:remove')")
    @Log(title = "仓库信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(wzCkService.deleteWzCkByIds(ids));
    }
}
