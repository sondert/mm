package com.ruoyi.wzck.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 仓库信息对象 wz_ck
 * 
 * @author sonder
 * @date 2021-11-14
 */
public class WzCk extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 仓库id */
    private Integer id;

    /** 仓库名 */
    @Excel(name = "仓库名")
    private String ckName;

    /** 仓库地址 */
    @Excel(name = "仓库地址")
    private String ckDz;

    /** 联系人 */
    @Excel(name = "联系人")
    private String ckLx;

    /** 电话 */
    @Excel(name = "电话")
    private String ckDh;

    /** 备注 */
    @Excel(name = "备注")
    private String ckBz;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setCkName(String ckName) 
    {
        this.ckName = ckName;
    }

    public String getCkName() 
    {
        return ckName;
    }
    public void setCkDz(String ckDz) 
    {
        this.ckDz = ckDz;
    }

    public String getCkDz() 
    {
        return ckDz;
    }
    public void setCkLx(String ckLx) 
    {
        this.ckLx = ckLx;
    }

    public String getCkLx() 
    {
        return ckLx;
    }
    public void setCkDh(String ckDh) 
    {
        this.ckDh = ckDh;
    }

    public String getCkDh() 
    {
        return ckDh;
    }
    public void setCkBz(String ckBz) 
    {
        this.ckBz = ckBz;
    }

    public String getCkBz() 
    {
        return ckBz;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ckName", getCkName())
            .append("ckDz", getCkDz())
            .append("ckLx", getCkLx())
            .append("ckDh", getCkDh())
            .append("ckBz", getCkBz())
            .toString();
    }
}
