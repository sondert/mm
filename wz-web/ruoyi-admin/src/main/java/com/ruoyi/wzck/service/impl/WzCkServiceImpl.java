package com.ruoyi.wzck.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wzck.mapper.WzCkMapper;
import com.ruoyi.wzck.domain.WzCk;
import com.ruoyi.wzck.service.IWzCkService;

/**
 * 仓库信息Service业务层处理
 * 
 * @author sonder
 * @date 2021-11-14
 */
@Service
public class WzCkServiceImpl implements IWzCkService 
{
    @Autowired
    private WzCkMapper wzCkMapper;

    /**
     * 查询仓库信息
     * 
     * @param id 仓库信息主键
     * @return 仓库信息
     */
    @Override
    public WzCk selectWzCkById(Integer id)
    {
        return wzCkMapper.selectWzCkById(id);
    }

    /**
     * 查询仓库信息列表
     * 
     * @param wzCk 仓库信息
     * @return 仓库信息
     */
    @Override
    public List<WzCk> selectWzCkList(WzCk wzCk)
    {
        return wzCkMapper.selectWzCkList(wzCk);
    }

    /**
     * 新增仓库信息
     * 
     * @param wzCk 仓库信息
     * @return 结果
     */
    @Override
    public int insertWzCk(WzCk wzCk)
    {
        return wzCkMapper.insertWzCk(wzCk);
    }

    /**
     * 修改仓库信息
     * 
     * @param wzCk 仓库信息
     * @return 结果
     */
    @Override
    public int updateWzCk(WzCk wzCk)
    {
        return wzCkMapper.updateWzCk(wzCk);
    }

    /**
     * 批量删除仓库信息
     * 
     * @param ids 需要删除的仓库信息主键
     * @return 结果
     */
    @Override
    public int deleteWzCkByIds(Integer[] ids)
    {
        return wzCkMapper.deleteWzCkByIds(ids);
    }

    /**
     * 删除仓库信息信息
     * 
     * @param id 仓库信息主键
     * @return 结果
     */
    @Override
    public int deleteWzCkById(Integer id)
    {
        return wzCkMapper.deleteWzCkById(id);
    }
}
