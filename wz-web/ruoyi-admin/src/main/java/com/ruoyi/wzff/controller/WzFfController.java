package com.ruoyi.wzff.controller;

import java.util.LinkedList;
import java.util.List;

import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.sdk.po.CkHis;
import com.ruoyi.sdk.util.CkUtil;

import com.ruoyi.wzck.domain.WzCk;
import com.ruoyi.wzck.service.IWzCkService;
import com.ruoyi.wzkc.domain.WzKc;
import com.ruoyi.wzkc.service.IWzKcService;
import com.ruoyi.wzmx.domain.WzMx;
import com.ruoyi.wzmx.service.IWzMxService;
import com.ruoyi.wzrk.domain.WzRk;

import com.ruoyi.wzwz.domain.WzWz;
import com.ruoyi.wzwz.service.IWzWzService;
import com.ruoyi.wzys.domain.KcVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wzff.domain.WzFf;
import com.ruoyi.wzff.service.IWzFfService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物资发放Controller
 * 
 * @author sonder
 * @date 2021-11-14
 */
@RestController
@RequestMapping("/wzff/wzff")
public class WzFfController extends BaseController
{
    @Autowired
    private IWzFfService wzFfService;

    @Autowired
    private CkUtil ckUtil;

    @Autowired
    private IWzCkService wzCkService;

    @Autowired
    private IWzKcService wzKcService;

    @Autowired
    private IWzWzService wzWzService;

    @Autowired
    private IWzMxService wzMxService;

    /**
     * 查询物资发放列表
     */
    @PreAuthorize("@ss.hasPermi('wzff:wzff:list')")
    @GetMapping("/list")
    public TableDataInfo list(WzFf wzFf)
    {
        startPage();
        List<WzFf> list = wzFfService.selectWzFfList(wzFf);
        return getDataTable(list);
    }
    @PreAuthorize("@ss.hasPermi('wzff:wzff:kc')")
    @GetMapping("/kc/{ckid}")
    public TableDataInfo kc(@PathVariable Integer ckid)
    {
        WzKc wzKc = new WzKc();
        wzKc.setCkCkid(ckid);
        List<WzKc> list = wzKcService.selectWzKcList(wzKc);
        if(list.size()==0){
            return getDataTable(new LinkedList<KcVo>());
        }
        List<KcVo> list2 = new LinkedList<KcVo>();
        for(WzKc kc:list){
            WzCk ck = wzCkService.selectWzCkById(kc.getCkCkid());
            WzWz wz = wzWzService.selectWzWzById(kc.getKcWzid());
            KcVo kcVo = new KcVo();
            kcVo.setId(kc.getId());
            kcVo.setCkCkName(ck.getCkName());
            kcVo.setKcTp(wz.getWzTp());
            kcVo.setKcWzname(wz.getWzName());
            kcVo.setKcGg(wz.getWzName());
            kcVo.setNum(kc.getNum());
            kcVo.setKcDw(wz.getWzDw());
            kcVo.setNownum(1);
            list2.add(kcVo);
        }
        return getDataTable(list2);
    }
    @PreAuthorize("@ss.hasPermi('wzff:wzff:bd')")
    @PutMapping("/bd")
    public AjaxResult bd()
    {
        List<WzFf> list = wzFfService.selectWzFfList(new WzFf());
        int rows =1;
        for (WzFf wzFf:list) {
            try {
                if(wzFf.getFfDh().equals(new Integer(3))){
                    break;
                }
                WzFf wzFf1 = ckUtil.query(wzFf.getFfDh());
                wzFf1.setId(wzFf.getId());
                if(!wzFf1.equals(wzFf)){
                    rows=0;
                    wzFfService.updateWzFf(wzFf1);
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("通过id查询区块链发放信息失败");
            }
        }
        return rows>0?AjaxResult.success("比对成功，数据一致"):AjaxResult.error("数据不一致，更新成功");
    }


    /**
     * 导出物资发放列表
     */
    @PreAuthorize("@ss.hasPermi('wzff:wzff:export')")
    @Log(title = "物资发放", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WzFf wzFf)
    {
        List<WzFf> list = wzFfService.selectWzFfList(wzFf);
        ExcelUtil<WzFf> util = new ExcelUtil<WzFf>(WzFf.class);
        return util.exportExcel(list, "物资发放数据");
    }

    /**
     * 获取物资发放详细信息
     */
    @PreAuthorize("@ss.hasPermi('wzff:wzff:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(wzFfService.selectWzFfById(id));
    }

    /**
     * 新增物资发放
     */
    @PreAuthorize("@ss.hasPermi('wzff:wzff:add')")
    @Log(title = "物资发放", businessType = BusinessType.INSERT)
    @PostMapping(value = "/{ids}/{nums}")
    public AjaxResult add(@RequestBody WzFf wzFf,@PathVariable Integer[] ids,@PathVariable Integer[] nums) {
        String dh = IdUtils.fastSimpleUUID();
        wzFf.setFfDh(dh);
        wzFf.setFfZt(1);
        int rows = wzFfService.insertWzFf(wzFf);
        if (rows > 0) {
            try {
                ckUtil.save(wzFf.getFfDh(), wzFf);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("物资发放区块链添加信息异常");
            }
        } else {
            System.out.println("物资发放添加信息失败");
        }
        if (rows > 0) {
            int i = 0;
            while (i < ids.length) {
                WzKc wzKc = wzKcService.selectWzKcById(ids[i]);
                WzWz wzWz = wzWzService.selectWzWzById(wzKc.getKcWzid());
                WzMx wzMx = new WzMx();
                wzMx.setMxDh(dh);
                wzMx.setMxMc(wzWz.getWzName());
                wzMx.setMxSpid(wzWz.getId());
                wzMx.setMxGg(wzWz.getWzGg());
                wzMx.setMxTp(wzWz.getWzTp());
                wzMx.setMxNum(nums[i]);
                wzMx.setMxDw(wzWz.getWzDw());
                wzMx.setMxType(3);
                int row2 = wzMxService.insertWzMx(wzMx);
                if (row2 <= 0) {
                    rows = 0;
                }
//                int row3 = 1;
//                wzKc.setNum(wzKc.getNum() - nums[i]);
//                row3 = wzKcService.updateWzKc(wzKc);
//                if (row3 <= 0) {
//                    rows = 0;
//                }
                i++;
            }
        }
        return toAjax(rows);
    }
    /**
     * 修改物资发放
     */
    @PreAuthorize("@ss.hasPermi('wzff:wzff:edit')")
    @Log(title = "物资发放", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WzFf wzFf)
    {
       try {
           ckUtil.save(wzFf.getFfDh(),wzFf);
       } catch (Exception e) {
           e.printStackTrace();
           System.out.println("修改物资发放信息时遇到异常");
       }
       return toAjax(wzFfService.updateWzFf(wzFf));
    }

    /**
     * 删除物资发放
     */
//    @PreAuthorize("@ss.hasPermi('wzff:wzff:remove')")
//    @Log(title = "物资发放", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Integer[] ids)
//    {
//        int rows = wzFfService.deleteWzFfByIds(ids);
//        if(rows>0){
//            for (int id:ids) {
//                WzFf wzFf = wzFfService.selectWzFfById(id);
//                try {
//                    ckUtil.delete(wzFf.getFfDh());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    System.out.println("批量删除物资发放信息时间遇到异常");
//                }
//            }
//        }else {
//            System.out.println("批量删除物资发放信息时失败");
//        }
//        return toAjax(rows);
//    }
    @PreAuthorize("@ss.hasPermi('wzff:wzff:remove')")
    @Log(title = "物资发放", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        int rows = 1;
        for (int id:ids) {
            WzFf wzFf = wzFfService.selectWzFfById(id);
            wzFf.setFfZt(3);
            int row = wzFfService.updateWzFf(wzFf);
            if(row<=0){
                rows=0;
            }
            if(rows>0){
                try {
                    ckUtil.delete(wzFf.getFfDh());
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("修改物资发放区块链信息时遇到异常");
                }
            }else {
                System.out.println("修改物资发放信息时失败");
            }
        }
        return toAjax(rows);
    }

    /**
     * 溯源
     */
    @PreAuthorize("@ss.hasPermi('wzff:wzff:trace')")
    @GetMapping(value = "/trace/{id}")
    public TableDataInfo trace(@PathVariable("id") Integer id)
    {
        List<CkHis> list = new LinkedList<>();
        try {
            CkHis[] ckHis = ckUtil.queryAll(wzFfService.selectWzFfById(id).getFfDh());
            for (CkHis his:ckHis) {
                if(Boolean.parseBoolean(his.IsDelete)){
                    WzFf wzFf = (WzFf) list.get(list.size()-1).THistory.clone();
                    wzFf.setFfZt(3);
                    his.setTHistory(wzFf);
                }
                list.add(his);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("查询物资发放历史信息失败");
        }
        return getDataTable(list);
    }

    /**
     * 发放
     */
    @PreAuthorize("@ss.hasPermi('wzff:wzff:ff')")
    @Log(title = "物资发放表", businessType = BusinessType.UPDATE)
    @PutMapping(value = "/ff/{id}")
    public AjaxResult ff(@PathVariable("id") Integer id)
    {
        WzFf wzFf = wzFfService.selectWzFfById(id);
        wzFf.setFfZt(2);
        int rows = wzFfService.updateWzFf(wzFf);
        if(rows>0){
            try {
                ckUtil.save(wzFf.getFfDh(),wzFf);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("修改物资发放区块链信息时遇到异常");
            }
        }else {
            System.out.println("修改物资发放信息时失败");
        }
        if(rows>0){
            WzMx wzMx = new WzMx();
            wzMx.setMxDh(wzFf.getFfDh());
            List<WzMx> list = wzMxService.selectWzMxList(wzMx);
            for (WzMx mx:list) {
                WzKc wzKc = new WzKc();
                wzKc.setCkCkid(wzFf.getFfCk());
                wzKc.setKcWzid(mx.getMxSpid());
                List<WzKc> kcs= wzKcService.selectWzKcList(wzKc);
                if(kcs.size()==1){
                    wzKc = kcs.get(0);
                    wzKc.setNum(wzKc.getNum()-mx.getMxNum());
                    int row = wzKcService.updateWzKc(wzKc);
                    if(row<=0){
                        rows=0;
                    }
                }else {
                    rows=0;
                }
            }
        }

        return toAjax(rows);
    }

    @PreAuthorize("@ss.hasPermi('wzff:wzff:mx')")
    @GetMapping(value = "/mx/{id}")
    public TableDataInfo mx(@PathVariable("id") Integer id)
    {
        WzMx wzMx = new WzMx();
        wzMx.setMxDh(wzFfService.selectWzFfById(id).getFfDh());
        List<WzMx> list = wzMxService.selectWzMxList(wzMx);
        List<WzMx> ans = new LinkedList<>();
        for (WzMx mx:list) {
            if(mx.getMxType()==3){
                ans.add(mx);
            }
        }
        return getDataTable(ans);
    }
}
