package com.ruoyi.wzff.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wzff.mapper.WzFfMapper;
import com.ruoyi.wzff.domain.WzFf;
import com.ruoyi.wzff.service.IWzFfService;

/**
 * 物资发放Service业务层处理
 * 
 * @author sonder
 * @date 2021-11-14
 */
@Service
public class WzFfServiceImpl implements IWzFfService 
{
    @Autowired
    private WzFfMapper wzFfMapper;

    /**
     * 查询物资发放
     * 
     * @param id 物资发放主键
     * @return 物资发放
     */
    @Override
    public WzFf selectWzFfById(Integer id)
    {
        return wzFfMapper.selectWzFfById(id);
    }

    /**
     * 查询物资发放列表
     * 
     * @param wzFf 物资发放
     * @return 物资发放
     */
    @Override
    public List<WzFf> selectWzFfList(WzFf wzFf)
    {
        return wzFfMapper.selectWzFfList(wzFf);
    }

    /**
     * 新增物资发放
     * 
     * @param wzFf 物资发放
     * @return 结果
     */
    @Override
    public int insertWzFf(WzFf wzFf)
    {
        return wzFfMapper.insertWzFf(wzFf);
    }

    /**
     * 修改物资发放
     * 
     * @param wzFf 物资发放
     * @return 结果
     */
    @Override
    public int updateWzFf(WzFf wzFf)
    {
        return wzFfMapper.updateWzFf(wzFf);
    }

    /**
     * 批量删除物资发放
     * 
     * @param ids 需要删除的物资发放主键
     * @return 结果
     */
    @Override
    public int deleteWzFfByIds(Integer[] ids)
    {
        return wzFfMapper.deleteWzFfByIds(ids);
    }

    /**
     * 删除物资发放信息
     * 
     * @param id 物资发放主键
     * @return 结果
     */
    @Override
    public int deleteWzFfById(Integer id)
    {
        return wzFfMapper.deleteWzFfById(id);
    }
}
