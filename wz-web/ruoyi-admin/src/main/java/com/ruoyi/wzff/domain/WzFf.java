package com.ruoyi.wzff.domain;

import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物资发放对象 wz_ff
 * 
 * @author sonder
 * @date 2021-11-14
 */
public class WzFf extends BaseEntity implements Cloneable
{
    private static final long serialVersionUID = 1L;

    /** 物资发放id */
    private Integer id;

    /** 发放单号 */
    @Excel(name = "发放单号")
    private String ffDh;

    /** 发放类型 */
    @Excel(name = "发放类型")
    private int ffType;

    /** 紧急程度 */
    @Excel(name = "紧急程度")
    private String ffCd;

    /** 收货方 */
    @Excel(name = "收货方")
    private Long ffSh;

    /** 数量 */
    @Excel(name = "数量")
    private Long ffNum;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String ffLx;

    /** 发放仓库 */
    @Excel(name = "发放仓库")
    private Integer ffCk;

    /** 状态 */
    @Excel(name = "状态")
    private int ffZt;

    /** 操作员 */
    @Excel(name = "操作员")
    private String ffCzy;

    /** 发放时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发放时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date ffTime;

    /** 备注 */
    @Excel(name = "备注")
    private String ffBz;

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId()
    {
        return id;
    }
    public void setFfDh(String ffDh) 
    {
        this.ffDh = ffDh;
    }

    public String getFfDh() 
    {
        return ffDh;
    }
    public void setFfType(int ffType)
    {
        this.ffType = ffType;
    }

    public int getFfType()
    {
        return ffType;
    }
    public void setFfCd(String ffCd) 
    {
        this.ffCd = ffCd;
    }

    public String getFfCd() 
    {
        return ffCd;
    }
    public void setFfSh(Long ffSh) 
    {
        this.ffSh = ffSh;
    }

    public Long getFfSh() 
    {
        return ffSh;
    }
    public void setFfNum(Long ffNum) 
    {
        this.ffNum = ffNum;
    }

    public Long getFfNum() 
    {
        return ffNum;
    }
    public void setFfLx(String ffLx) 
    {
        this.ffLx = ffLx;
    }

    public String getFfLx() 
    {
        return ffLx;
    }
    public void setFfZt(int ffZt)
    {
        this.ffZt = ffZt;
    }

    public int getFfZt()
    {
        return ffZt;
    }
    public void setFfCzy(String ffCzy) 
    {
        this.ffCzy = ffCzy;
    }

    public String getFfCzy() 
    {
        return ffCzy;
    }
    public void setFfTime(Date ffTime) 
    {
        this.ffTime = ffTime;
    }

    public Date getFfTime() 
    {
        return ffTime;
    }
    public void setFfBz(String ffBz) 
    {
        this.ffBz = ffBz;
    }

    public Integer getFfCk() {
        return ffCk;
    }

    public void setFfCk(Integer ffCk) {
        this.ffCk = ffCk;
    }

    public String getFfBz()
    {
        return ffBz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WzFf wzFf = (WzFf) o;
        return ffType == wzFf.ffType && ffZt == wzFf.ffZt && Objects.equals(id, wzFf.id) && Objects.equals(ffDh, wzFf.ffDh) && Objects.equals(ffCd, wzFf.ffCd) && Objects.equals(ffSh, wzFf.ffSh) && Objects.equals(ffNum, wzFf.ffNum) && Objects.equals(ffLx, wzFf.ffLx) && Objects.equals(ffCk, wzFf.ffCk) && Objects.equals(ffCzy, wzFf.ffCzy) && Objects.equals(ffBz, wzFf.ffBz);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ffDh, ffType, ffCd, ffSh, ffNum, ffLx, ffCk, ffZt, ffCzy, ffBz);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ffDh", getFfDh())
            .append("ffType", getFfType())
            .append("ffCd", getFfCd())
            .append("ffSh", getFfSh())
            .append("ffNum", getFfNum())
            .append("ffLx", getFfLx())
            .append("ffZt", getFfZt())
            .append("ffCzy", getFfCzy())
            .append("ffTime", getFfTime())
            .append("ffBz", getFfBz())
            .toString();
    }
}
