package com.ruoyi.wzff.mapper;

import java.util.List;
import com.ruoyi.wzff.domain.WzFf;

/**
 * 物资发放Mapper接口
 * 
 * @author sonder
 * @date 2021-11-14
 */
public interface WzFfMapper 
{
    /**
     * 查询物资发放
     * 
     * @param id 物资发放主键
     * @return 物资发放
     */
    public WzFf selectWzFfById(Integer id);

    /**
     * 查询物资发放列表
     * 
     * @param wzFf 物资发放
     * @return 物资发放集合
     */
    public List<WzFf> selectWzFfList(WzFf wzFf);

    /**
     * 新增物资发放
     * 
     * @param wzFf 物资发放
     * @return 结果
     */
    public int insertWzFf(WzFf wzFf);

    /**
     * 修改物资发放
     * 
     * @param wzFf 物资发放
     * @return 结果
     */
    public int updateWzFf(WzFf wzFf);

    /**
     * 删除物资发放
     * 
     * @param id 物资发放主键
     * @return 结果
     */
    public int deleteWzFfById(Integer id);

    /**
     * 批量删除物资发放
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWzFfByIds(Integer[] ids);
}
