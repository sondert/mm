package com.ruoyi.wzrz.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物资日志对象 wz_rz
 * 
 * @author sonder
 * @date 2021-11-14
 */
public class WzRz extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 日志id */
    private Long id;

    /** 日志种类 */
    @Excel(name = "日志种类")
    private String rzType;

    /** 操作人员 */
    @Excel(name = "操作人员")
    private String rzRy;

    /** 操作地址 */
    @Excel(name = "操作地址")
    private String rzDz;

    /** 操作地点 */
    @Excel(name = "操作地点")
    private String rzDd;

    /** 操作状态 */
    @Excel(name = "操作状态")
    private String rzZt;

    /** 操作日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "操作日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date rzRq;

    /** 操作描述 */
    @Excel(name = "操作描述")
    private String rzMs;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setRzType(String rzType) 
    {
        this.rzType = rzType;
    }

    public String getRzType() 
    {
        return rzType;
    }
    public void setRzRy(String rzRy) 
    {
        this.rzRy = rzRy;
    }

    public String getRzRy() 
    {
        return rzRy;
    }
    public void setRzDz(String rzDz) 
    {
        this.rzDz = rzDz;
    }

    public String getRzDz() 
    {
        return rzDz;
    }
    public void setRzDd(String rzDd) 
    {
        this.rzDd = rzDd;
    }

    public String getRzDd() 
    {
        return rzDd;
    }
    public void setRzZt(String rzZt) 
    {
        this.rzZt = rzZt;
    }

    public String getRzZt() 
    {
        return rzZt;
    }
    public void setRzRq(Date rzRq) 
    {
        this.rzRq = rzRq;
    }

    public Date getRzRq() 
    {
        return rzRq;
    }
    public void setRzMs(String rzMs) 
    {
        this.rzMs = rzMs;
    }

    public String getRzMs() 
    {
        return rzMs;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("rzType", getRzType())
            .append("rzRy", getRzRy())
            .append("rzDz", getRzDz())
            .append("rzDd", getRzDd())
            .append("rzZt", getRzZt())
            .append("rzRq", getRzRq())
            .append("rzMs", getRzMs())
            .toString();
    }
}
