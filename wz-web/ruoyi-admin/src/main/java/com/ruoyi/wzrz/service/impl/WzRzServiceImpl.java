package com.ruoyi.wzrz.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wzrz.mapper.WzRzMapper;
import com.ruoyi.wzrz.domain.WzRz;
import com.ruoyi.wzrz.service.IWzRzService;

/**
 * 物资日志Service业务层处理
 * 
 * @author sonder
 * @date 2021-11-14
 */
@Service
public class WzRzServiceImpl implements IWzRzService 
{
    @Autowired
    private WzRzMapper wzRzMapper;

    /**
     * 查询物资日志
     * 
     * @param id 物资日志主键
     * @return 物资日志
     */
    @Override
    public WzRz selectWzRzById(Long id)
    {
        return wzRzMapper.selectWzRzById(id);
    }

    /**
     * 查询物资日志列表
     * 
     * @param wzRz 物资日志
     * @return 物资日志
     */
    @Override
    public List<WzRz> selectWzRzList(WzRz wzRz)
    {
        return wzRzMapper.selectWzRzList(wzRz);
    }

    /**
     * 新增物资日志
     * 
     * @param wzRz 物资日志
     * @return 结果
     */
    @Override
    public int insertWzRz(WzRz wzRz)
    {
        return wzRzMapper.insertWzRz(wzRz);
    }

    /**
     * 修改物资日志
     * 
     * @param wzRz 物资日志
     * @return 结果
     */
    @Override
    public int updateWzRz(WzRz wzRz)
    {
        return wzRzMapper.updateWzRz(wzRz);
    }

    /**
     * 批量删除物资日志
     * 
     * @param ids 需要删除的物资日志主键
     * @return 结果
     */
    @Override
    public int deleteWzRzByIds(Long[] ids)
    {
        return wzRzMapper.deleteWzRzByIds(ids);
    }

    /**
     * 删除物资日志信息
     * 
     * @param id 物资日志主键
     * @return 结果
     */
    @Override
    public int deleteWzRzById(Long id)
    {
        return wzRzMapper.deleteWzRzById(id);
    }
}
