package com.ruoyi.wzrz.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wzrz.domain.WzRz;
import com.ruoyi.wzrz.service.IWzRzService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物资日志Controller
 * 
 * @author sonder
 * @date 2021-11-14
 */
@RestController
@RequestMapping("/wzrz/wzrz")
public class WzRzController extends BaseController
{
    @Autowired
    private IWzRzService wzRzService;

    /**
     * 查询物资日志列表
     */
    @PreAuthorize("@ss.hasPermi('wzrz:wzrz:list')")
    @GetMapping("/list")
    public TableDataInfo list(WzRz wzRz)
    {
        startPage();
        List<WzRz> list = wzRzService.selectWzRzList(wzRz);
        return getDataTable(list);
    }

    /**
     * 导出物资日志列表
     */
    @PreAuthorize("@ss.hasPermi('wzrz:wzrz:export')")
    @Log(title = "物资日志", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WzRz wzRz)
    {
        List<WzRz> list = wzRzService.selectWzRzList(wzRz);
        ExcelUtil<WzRz> util = new ExcelUtil<WzRz>(WzRz.class);
        return util.exportExcel(list, "物资日志数据");
    }

    /**
     * 获取物资日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('wzrz:wzrz:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(wzRzService.selectWzRzById(id));
    }

    /**
     * 新增物资日志
     */
    @PreAuthorize("@ss.hasPermi('wzrz:wzrz:add')")
    @Log(title = "物资日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WzRz wzRz)
    {
        return toAjax(wzRzService.insertWzRz(wzRz));
    }

    /**
     * 修改物资日志
     */
    @PreAuthorize("@ss.hasPermi('wzrz:wzrz:edit')")
    @Log(title = "物资日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WzRz wzRz)
    {
        return toAjax(wzRzService.updateWzRz(wzRz));
    }

    /**
     * 删除物资日志
     */
    @PreAuthorize("@ss.hasPermi('wzrz:wzrz:remove')")
    @Log(title = "物资日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wzRzService.deleteWzRzByIds(ids));
    }
}
