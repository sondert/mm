package com.ruoyi.wzrz.service;

import java.util.List;
import com.ruoyi.wzrz.domain.WzRz;

/**
 * 物资日志Service接口
 * 
 * @author sonder
 * @date 2021-11-14
 */
public interface IWzRzService 
{
    /**
     * 查询物资日志
     * 
     * @param id 物资日志主键
     * @return 物资日志
     */
    public WzRz selectWzRzById(Long id);

    /**
     * 查询物资日志列表
     * 
     * @param wzRz 物资日志
     * @return 物资日志集合
     */
    public List<WzRz> selectWzRzList(WzRz wzRz);

    /**
     * 新增物资日志
     * 
     * @param wzRz 物资日志
     * @return 结果
     */
    public int insertWzRz(WzRz wzRz);

    /**
     * 修改物资日志
     * 
     * @param wzRz 物资日志
     * @return 结果
     */
    public int updateWzRz(WzRz wzRz);

    /**
     * 批量删除物资日志
     * 
     * @param ids 需要删除的物资日志主键集合
     * @return 结果
     */
    public int deleteWzRzByIds(Long[] ids);

    /**
     * 删除物资日志信息
     * 
     * @param id 物资日志主键
     * @return 结果
     */
    public int deleteWzRzById(Long id);
}
