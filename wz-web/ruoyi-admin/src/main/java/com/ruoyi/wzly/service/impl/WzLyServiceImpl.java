package com.ruoyi.wzly.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wzly.mapper.WzLyMapper;
import com.ruoyi.wzly.domain.WzLy;
import com.ruoyi.wzly.service.IWzLyService;

/**
 * 物资来源Service业务层处理
 * 
 * @author sonder
 * @date 2021-11-14
 */
@Service
public class WzLyServiceImpl implements IWzLyService 
{
    @Autowired
    private WzLyMapper wzLyMapper;

    /**
     * 查询物资来源
     * 
     * @param id 物资来源主键
     * @return 物资来源
     */
    @Override
    public WzLy selectWzLyById(Integer id)
    {
        return wzLyMapper.selectWzLyById(id);
    }

    /**
     * 查询物资来源列表
     * 
     * @param wzLy 物资来源
     * @return 物资来源
     */
    @Override
    public List<WzLy> selectWzLyList(WzLy wzLy)
    {
        return wzLyMapper.selectWzLyList(wzLy);
    }

    /**
     * 新增物资来源
     * 
     * @param wzLy 物资来源
     * @return 结果
     */
    @Override
    public int insertWzLy(WzLy wzLy)
    {
        return wzLyMapper.insertWzLy(wzLy);
    }

    /**
     * 修改物资来源
     * 
     * @param wzLy 物资来源
     * @return 结果
     */
    @Override
    public int updateWzLy(WzLy wzLy)
    {
        return wzLyMapper.updateWzLy(wzLy);
    }

    /**
     * 批量删除物资来源
     * 
     * @param ids 需要删除的物资来源主键
     * @return 结果
     */
    @Override
    public int deleteWzLyByIds(Integer[] ids)
    {
        return wzLyMapper.deleteWzLyByIds(ids);
    }

    /**
     * 删除物资来源信息
     * 
     * @param id 物资来源主键
     * @return 结果
     */
    @Override
    public int deleteWzLyById(Integer id)
    {
        return wzLyMapper.deleteWzLyById(id);
    }
}
