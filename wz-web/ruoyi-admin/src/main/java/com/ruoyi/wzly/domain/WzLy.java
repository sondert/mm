package com.ruoyi.wzly.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物资来源对象 wz_ly
 * 
 * @author sonder
 * @date 2021-11-14
 */
public class WzLy extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 来源id */
    private Integer id;

    /** 省份 */
    @Excel(name = "省份")
    private String lySf;

    /** 市 */
    @Excel(name = "市")
    private String lyS;

    /** 区县 */
    @Excel(name = "区县")
    private String lyQx;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String lyXx;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lyTime;

    /** 邮箱 */
    @Excel(name = "邮箱")
    private String lyYx;

    /** 联系人 */
    @Excel(name = "联系人")
    private String lyLx;

    /** 电话 */
    @Excel(name = "电话")
    private String lyDh;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setLySf(String lySf) 
    {
        this.lySf = lySf;
    }

    public String getLySf() 
    {
        return lySf;
    }
    public void setLyS(String lyS) 
    {
        this.lyS = lyS;
    }

    public String getLyS() 
    {
        return lyS;
    }
    public void setLyQx(String lyQx) 
    {
        this.lyQx = lyQx;
    }

    public String getLyQx() 
    {
        return lyQx;
    }
    public void setLyXx(String lyXx) 
    {
        this.lyXx = lyXx;
    }

    public String getLyXx() 
    {
        return lyXx;
    }
    public void setLyTime(Date lyTime) 
    {
        this.lyTime = lyTime;
    }

    public Date getLyTime() 
    {
        return lyTime;
    }
    public void setLyYx(String lyYx) 
    {
        this.lyYx = lyYx;
    }

    public String getLyYx() 
    {
        return lyYx;
    }
    public void setLyLx(String lyLx) 
    {
        this.lyLx = lyLx;
    }

    public String getLyLx() 
    {
        return lyLx;
    }
    public void setLyDh(String lyDh) 
    {
        this.lyDh = lyDh;
    }

    public String getLyDh() 
    {
        return lyDh;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("lySf", getLySf())
            .append("lyS", getLyS())
            .append("lyQx", getLyQx())
            .append("lyXx", getLyXx())
            .append("lyTime", getLyTime())
            .append("lyYx", getLyYx())
            .append("lyLx", getLyLx())
            .append("lyDh", getLyDh())
            .toString();
    }
}
