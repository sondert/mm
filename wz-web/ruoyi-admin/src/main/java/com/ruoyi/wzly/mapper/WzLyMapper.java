package com.ruoyi.wzly.mapper;

import java.util.List;
import com.ruoyi.wzly.domain.WzLy;

/**
 * 物资来源Mapper接口
 * 
 * @author sonder
 * @date 2021-11-14
 */
public interface WzLyMapper 
{
    /**
     * 查询物资来源
     * 
     * @param id 物资来源主键
     * @return 物资来源
     */
    public WzLy selectWzLyById(Integer id);

    /**
     * 查询物资来源列表
     * 
     * @param wzLy 物资来源
     * @return 物资来源集合
     */
    public List<WzLy> selectWzLyList(WzLy wzLy);

    /**
     * 新增物资来源
     * 
     * @param wzLy 物资来源
     * @return 结果
     */
    public int insertWzLy(WzLy wzLy);

    /**
     * 修改物资来源
     * 
     * @param wzLy 物资来源
     * @return 结果
     */
    public int updateWzLy(WzLy wzLy);

    /**
     * 删除物资来源
     * 
     * @param id 物资来源主键
     * @return 结果
     */
    public int deleteWzLyById(Integer id);

    /**
     * 批量删除物资来源
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWzLyByIds(Integer[] ids);
}
