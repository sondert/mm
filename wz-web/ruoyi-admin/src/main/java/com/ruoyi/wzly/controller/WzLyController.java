package com.ruoyi.wzly.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wzly.domain.WzLy;
import com.ruoyi.wzly.service.IWzLyService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物资来源Controller
 * 
 * @author sonder
 * @date 2021-11-14
 */
@RestController
@RequestMapping("/wzly/wzly")
public class WzLyController extends BaseController
{
    @Autowired
    private IWzLyService wzLyService;

    /**
     * 查询物资来源列表
     */
    @PreAuthorize("@ss.hasPermi('wzly:wzly:list')")
    @GetMapping("/list")
    public TableDataInfo list(WzLy wzLy)
    {
        startPage();
        List<WzLy> list = wzLyService.selectWzLyList(wzLy);
        return getDataTable(list);
    }

    /**
     * 导出物资来源列表
     */
    @PreAuthorize("@ss.hasPermi('wzly:wzly:export')")
    @Log(title = "物资来源", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WzLy wzLy)
    {
        List<WzLy> list = wzLyService.selectWzLyList(wzLy);
        ExcelUtil<WzLy> util = new ExcelUtil<WzLy>(WzLy.class);
        return util.exportExcel(list, "物资来源数据");
    }

    /**
     * 获取物资来源详细信息
     */
    @PreAuthorize("@ss.hasPermi('wzly:wzly:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(wzLyService.selectWzLyById(id));
    }

    /**
     * 新增物资来源
     */
    @PreAuthorize("@ss.hasPermi('wzly:wzly:add')")
    @Log(title = "物资来源", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WzLy wzLy)
    {
        return toAjax(wzLyService.insertWzLy(wzLy));
    }

    /**
     * 修改物资来源
     */
    @PreAuthorize("@ss.hasPermi('wzly:wzly:edit')")
    @Log(title = "物资来源", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WzLy wzLy)
    {
        return toAjax(wzLyService.updateWzLy(wzLy));
    }

    /**
     * 删除物资来源
     */
    @PreAuthorize("@ss.hasPermi('wzly:wzly:remove')")
    @Log(title = "物资来源", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(wzLyService.deleteWzLyByIds(ids));
    }
}
