package com.ruoyi.wzrk.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wzrk.mapper.WzRkMapper;
import com.ruoyi.wzrk.domain.WzRk;
import com.ruoyi.wzrk.service.IWzRkService;

/**
 * 物资入库表Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-14
 */
@Service
public class WzRkServiceImpl implements IWzRkService 
{
    @Autowired
    private WzRkMapper wzRkMapper;

    /**
     * 查询物资入库表
     * 
     * @param id 物资入库表主键
     * @return 物资入库表
     */
    @Override
    public WzRk selectWzRkById(Integer id)
    {
        return wzRkMapper.selectWzRkById(id);
    }

    /**
     * 查询物资入库表列表
     * 
     * @param wzRk 物资入库表
     * @return 物资入库表
     */
    @Override
    public List<WzRk> selectWzRkList(WzRk wzRk)
    {
        return wzRkMapper.selectWzRkList(wzRk);
    }

    /**
     * 新增物资入库表
     * 
     * @param wzRk 物资入库表
     * @return 结果
     */
    @Override
    public int insertWzRk(WzRk wzRk)
    {
        return wzRkMapper.insertWzRk(wzRk);
    }

    /**
     * 修改物资入库表
     * 
     * @param wzRk 物资入库表
     * @return 结果
     */
    @Override
    public int updateWzRk(WzRk wzRk)
    {
        return wzRkMapper.updateWzRk(wzRk);
    }

    /**
     * 批量删除物资入库表
     * 
     * @param ids 需要删除的物资入库表主键
     * @return 结果
     */
    @Override
    public int deleteWzRkByIds(Integer[] ids)
    {
        return wzRkMapper.deleteWzRkByIds(ids);
    }

    /**
     * 删除物资入库表信息
     * 
     * @param id 物资入库表主键
     * @return 结果
     */
    @Override
    public int deleteWzRkById(Integer id)
    {
        return wzRkMapper.deleteWzRkById(id);
    }
}
