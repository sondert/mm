package com.ruoyi.wzrk.mapper;

import java.util.List;
import com.ruoyi.wzrk.domain.WzRk;

/**
 * 物资入库表Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-14
 */
public interface WzRkMapper 
{
    /**
     * 查询物资入库表
     * 
     * @param id 物资入库表主键
     * @return 物资入库表
     */
    public WzRk selectWzRkById(Integer id);

    /**
     * 查询物资入库表列表
     * 
     * @param wzRk 物资入库表
     * @return 物资入库表集合
     */
    public List<WzRk> selectWzRkList(WzRk wzRk);

    /**
     * 新增物资入库表
     * 
     * @param wzRk 物资入库表
     * @return 结果
     */
    public int insertWzRk(WzRk wzRk);

    /**
     * 修改物资入库表
     * 
     * @param wzRk 物资入库表
     * @return 结果
     */
    public int updateWzRk(WzRk wzRk);

    /**
     * 删除物资入库表
     * 
     * @param id 物资入库表主键
     * @return 结果
     */
    public int deleteWzRkById(Integer id);

    /**
     * 批量删除物资入库表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWzRkByIds(Integer[] ids);
}
