package com.ruoyi.wzrk.controller;

import java.util.LinkedList;
import java.util.List;

import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.sdk.po.RKHis;
import com.ruoyi.sdk.util.RkUtil;
import com.ruoyi.wzkc.domain.WzKc;
import com.ruoyi.wzkc.service.IWzKcService;
import com.ruoyi.wzmx.domain.WzMx;
import com.ruoyi.wzmx.service.IWzMxService;
import com.ruoyi.wzrk.domain.RkVo;
import com.ruoyi.wzwz.domain.WzWz;
import com.ruoyi.wzwz.service.IWzWzService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wzrk.domain.WzRk;
import com.ruoyi.wzrk.service.IWzRkService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物资入库表Controller
 * 
 * @author ruoyi
 * @date 2021-11-14
 */
@RestController
@RequestMapping("/wzrk/wzrk")
public class WzRkController extends BaseController
{
    @Autowired
    private IWzRkService wzRkService;

    @Autowired
    private RkUtil rkUtil;

    @Autowired
    private IWzWzService wzWzService;

    @Autowired
    private IWzMxService wzMxService;

    @Autowired
    private IWzKcService wzKcService;
    /**
     * 查询物资入库表列表
     */
    @PreAuthorize("@ss.hasPermi('wzrk:wzrk:list')")
    @GetMapping("/list")
    public TableDataInfo list(WzRk wzRk)
    {
        startPage();
        List<WzRk> list = wzRkService.selectWzRkList(wzRk);
        return getDataTable(list);
    }
    @PreAuthorize("@ss.hasPermi('wzrk:wzrk:bd')")
    @PutMapping("/bd")
    public AjaxResult bd()
    {
        List<WzRk> list = wzRkService.selectWzRkList(new WzRk());
        int rows =1;
        for (WzRk wzRK:list) {
            try {
                if(wzRK.getRkDh().equals(new Integer(3))){
                    break;
                }
                WzRk wzRK1 = rkUtil.query(wzRK.getRkDh());
                wzRK1.setId(wzRK.getId());
                if(!wzRK1.equals(wzRK)){
                    rows=0;
                    wzRkService.updateWzRk(wzRK1);
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("通过id查询区块链入库信息失败");
            }
        }
        return rows>0?AjaxResult.success("比对成功，数据一致"):AjaxResult.error("数据不一致，更新成功");
    }

    /**
     * 导出物资入库表列表
     */
    @PreAuthorize("@ss.hasPermi('wzrk:wzrk:export')")
    @Log(title = "物资入库表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WzRk wzRk)
    {
        List<WzRk> list = wzRkService.selectWzRkList(wzRk);
        ExcelUtil<WzRk> util = new ExcelUtil<WzRk>(WzRk.class);
        return util.exportExcel(list, "物资入库表数据");
    }

    /**
     * 获取物资入库表详细信息
     */
    @PreAuthorize("@ss.hasPermi('wzrk:wzrk:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(wzRkService.selectWzRkById(id));
    }

    /**
     * 新增物资入库表
     */
    @PreAuthorize("@ss.hasPermi('wzrk:wzrk:add')")
    @Log(title = "物资入库表", businessType = BusinessType.INSERT)
    @PostMapping(value = "/{ids}/{nums}")
    public AjaxResult add(@RequestBody WzRk wzRk,@PathVariable Integer[] ids,@PathVariable Integer[] nums)
    {

        String dh = IdUtils.fastSimpleUUID();
        wzRk.setRkDh(dh);
        wzRk.setRkZt(1);
        int rows = wzRkService.insertWzRk(wzRk);
        if(rows>0){
            try {
                rkUtil.save(wzRk.getRkDh(),wzRk);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("物资入库区块链添加信息异常");
            }
        }else{
            System.out.println("物资入库区块链添加信息失败");
        }
        if(rows>0){
            int i=0;
            while (i<ids.length){
                WzWz wz = wzWzService.selectWzWzById(ids[i]);
                WzMx wzMx = new WzMx();
                wzMx.setMxDh(dh);
                wzMx.setMxMc(wz.getWzName());
                wzMx.setMxSpid(wz.getId());
                wzMx.setMxGg(wz.getWzGg());
                wzMx.setMxTp(wz.getWzTp());
                wzMx.setMxNum(nums[i]);
                wzMx.setMxDw(wz.getWzDw());
                wzMx.setMxType(1);
                int row2 = wzMxService.insertWzMx(wzMx);
                if(row2<=0){
                    rows=0;
                }
//                WzKc kc = new WzKc();
//                kc.setCkCkid(wzRk.getRkCk());
//                kc.setKcWzid(wz.getId());
//                List<WzKc> kcs = wzKcService.selectWzKcList(kc);
//                WzKc wzKc = new WzKc();
//                int row3=1;
//                if(kcs.size()==1){
//                    wzKc.setNum(kcs.get(0).getNum()+nums[i]);
//                    wzKc.setId(kcs.get(0).getId());
//                    row3 = wzKcService.updateWzKc(wzKc);
//                }else {
//                    wzKc.setCkCkid(wzRk.getRkCk());
//                    wzKc.setKcWzid(wz.getId());
//                    wzKc.setNum(nums[i]);
//                    row3 = wzKcService.insertWzKc(wzKc);
//                }
//                if(row3<=0){
//                    rows=0;
//                }
                i++;
            }
        }
        return toAjax(rows);
    }

    /**
     * 修改物资入库表
     */
    @PreAuthorize("@ss.hasPermi('wzrk:wzrk:edit')")
    @Log(title = "物资入库表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WzRk wzRk)
    {
        int rows = wzRkService.updateWzRk(wzRk);
        if(rows>0){
            try {
                rkUtil.save(wzRk.getRkDh(),wzRk);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("修改物资入库区块链信息时遇到异常");
            }
        }else {
            System.out.println("修改物资入库信息时失败");
        }
        return toAjax(rows);
    }


    /**
     * 删除物资入库表
     */
//    @PreAuthorize("@ss.hasPermi('wzrk:wzrk:remove')")
//    @Log(title = "物资入库表", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Integer[] ids)
//    {
//        for (int id:ids) {
//            WzRk wzRk = wzRkService.selectWzRkById(id);
//            try {
//                rkUtil.delete(wzRk.getRkDh());
//            } catch (Exception e) {
//                e.printStackTrace();
//                System.out.println("批量删除物资入库信息时间遇到异常");
//            }
//        }
//        return toAjax(wzRkService.deleteWzRkByIds(ids));
//    }
    @PreAuthorize("@ss.hasPermi('wzrk:wzrk:remove')")
    @Log(title = "物资入库表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)  {
        int rows = 1;
        for (int id:ids) {
            WzRk wzRk = wzRkService.selectWzRkById(id);
            wzRk.setRkZt(3);
            int row = wzRkService.updateWzRk(wzRk);
            if(row<=0){
                rows=0;
            }
            if(rows>0){
                try {
                    rkUtil.delete(wzRk.getRkDh());
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("修改物资入库区块链信息时遇到异常");
                }
            }else {
                System.out.println("修改物资入库信息时失败");
            }
        }
        return toAjax(rows);
    }

    /**
     * 溯源
     */
    @PreAuthorize("@ss.hasPermi('wzrk:wzrk:trace')")
    @GetMapping(value = "/trace/{id}")
    public TableDataInfo trace(@PathVariable("id") Integer id)
    {
        List<RKHis> list = new LinkedList<>();
        try {
            RKHis[] rkHis = rkUtil.queryAll(wzRkService.selectWzRkById(id).getRkDh());

            for (RKHis his:rkHis) {
                if(Boolean.parseBoolean(his.IsDelete)){
                        WzRk wzrk = (WzRk) list.get(list.size()-1).THistory.clone();
                        wzrk.setRkZt(3);
                        his.setTHistory(wzrk);
                }
                list.add(his);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("查询物资入库历史信息失败");
        }
        return getDataTable(list);
    }

    /**
     * 入库
     */
    @PreAuthorize("@ss.hasPermi('wzrk:wzrk:rk')")
    @Log(title = "物资入库表", businessType = BusinessType.UPDATE)
    @PutMapping(value = "/rk/{id}")
    public AjaxResult rk(@PathVariable("id") Integer id)
    {
        WzRk wzRk = wzRkService.selectWzRkById(id);
        wzRk.setRkZt(2);
        int rows = wzRkService.updateWzRk(wzRk);
        if(rows>0){
            try {
                rkUtil.save(wzRk.getRkDh(),wzRk);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("修改物资入库区块链信息时遇到异常");
            }
        }else {
            System.out.println("修改物资入库信息时失败");
        }
        if(rows>0){
            WzMx wzMx = new WzMx();
            wzMx.setMxDh(wzRk.getRkDh());
            List<WzMx> list = wzMxService.selectWzMxList(wzMx);
            for (WzMx mx:list) {
                WzKc wzKc = new WzKc();
                wzKc.setCkCkid(wzRk.getRkCk());
                wzKc.setKcWzid(mx.getMxSpid());
                List<WzKc> kcs= wzKcService.selectWzKcList(wzKc);
                WzKc wzKc1 = new WzKc();
                int row=1;
                if(kcs.size()==1){
                    wzKc1 = kcs.get(0);
                    wzKc1.setNum(wzKc1.getNum()+mx.getMxNum());
                    row = wzKcService.updateWzKc(wzKc1);
                }else {
                    wzKc1.setNum(wzMx.getMxNum());
                    wzKc1.setCkCkid(wzRk.getRkCk());
                    wzKc1.setKcWzid(wzMx.getMxSpid());
                    row = wzKcService.insertWzKc(wzKc1);
                }
                if(row<=0){
                    rows=0;
                }
            }
        }
        return toAjax(rows);
    }

    @PreAuthorize("@ss.hasPermi('wzrk:wzrk:wz')")
    @GetMapping("/wz")
    public TableDataInfo wz()
    {
        startPage();
        List<WzWz> list = wzWzService.selectWzWzList(new WzWz());
        List<RkVo> list2 = new LinkedList<RkVo>();
        for (WzWz wz: list) {
            if(wz.getWzZt()==2){
                RkVo rkVo = new RkVo();
                rkVo.setId(wz.getId());
                rkVo.setWzDw(wz.getWzDw());
                rkVo.setWzGg(wz.getWzGg());
                rkVo.setWzName(wz.getWzName());
                rkVo.setWzTp(wz.getWzTp());
                rkVo.setNum(1);
                list2.add(rkVo);
            }
        }
        return getDataTable(list2);
    }


    @PreAuthorize("@ss.hasPermi('wzrk:wzrk:query')")
    @GetMapping(value = "/mx/{id}")
    public TableDataInfo mx(@PathVariable("id") Integer id)
    {
        WzMx wzMx = new WzMx();
        wzMx.setMxDh(wzRkService.selectWzRkById(id).getRkDh());
        List<WzMx> list = wzMxService.selectWzMxList(wzMx);
        List<WzMx> ans = new LinkedList<>();
        for (WzMx mx:list) {
            if(mx.getMxType()==1){
                ans.add(mx);
            }
        }
        return getDataTable(ans);
    }
}
