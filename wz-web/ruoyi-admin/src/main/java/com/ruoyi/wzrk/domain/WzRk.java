package com.ruoyi.wzrk.domain;

import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物资入库表对象 wz_rk
 * 
 * @author ruoyi
 * @date 2021-11-14
 */
public class WzRk extends BaseEntity implements Cloneable
{
    private static final long serialVersionUID = 1L;

    /** 入库id */
    private Integer id;

    /** 电话 */
    @Excel(name = "单号")
    private String rkDh;

    /** 种类 */
    @Excel(name = "种类")
    private int rkType;

    /** 数量 */
    @Excel(name = "数量")
    private Integer rkSl;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String rkFs;

    /** 入库仓库 */
    @Excel(name = "联系方式")
    private Integer rkCk;

    /** 状态 */
    @Excel(name = "状态")
    private int rkZt;

    /** 操作员 */
    @Excel(name = "操作员")
    private String rkCzry;

    /** 物资提供方 */
    @Excel(name = "物资提供方")
    private Integer rkTg;

    /** 入库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "入库时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date rkTime;

    /** 备注 */
    @Excel(name = "备注")
    private String rkBz;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setRkDh(String rkDh) 
    {
        this.rkDh = rkDh;
    }

    public String getRkDh() 
    {
        return rkDh;
    }
    public void setRkType(int rkType)
    {
        this.rkType = rkType;
    }

    public int getRkType()
    {
        return rkType;
    }
    public void setRkSl(Integer rkSl) 
    {
        this.rkSl = rkSl;
    }

    public Integer getRkSl() 
    {
        return rkSl;
    }
    public void setRkFs(String rkFs) 
    {
        this.rkFs = rkFs;
    }

    public String getRkFs() 
    {
        return rkFs;
    }
    public void setRkZt(int rkZt)
    {
        this.rkZt = rkZt;
    }

    public int getRkZt()
    {
        return rkZt;
    }
    public void setRkCzry(String rkCzry) 
    {
        this.rkCzry = rkCzry;
    }

    public String getRkCzry() 
    {
        return rkCzry;
    }
    public void setRkTg(Integer rkTg) 
    {
        this.rkTg = rkTg;
    }

    public Integer getRkTg() 
    {
        return rkTg;
    }
    public void setRkTime(Date rkTime) 
    {
        this.rkTime = rkTime;
    }

    public Date getRkTime() 
    {
        return rkTime;
    }
    public void setRkBz(String rkBz) 
    {
        this.rkBz = rkBz;
    }

    public String getRkBz() 
    {
        return rkBz;
    }

    public Integer getRkCk() {
        return rkCk;
    }

    public void setRkCk(Integer rkCk) {
        this.rkCk = rkCk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WzRk wzRk = (WzRk) o;
        return rkType == wzRk.rkType && rkZt == wzRk.rkZt && Objects.equals(id, wzRk.id) && Objects.equals(rkDh, wzRk.rkDh) && Objects.equals(rkSl, wzRk.rkSl) && Objects.equals(rkFs, wzRk.rkFs) && Objects.equals(rkCk, wzRk.rkCk) && Objects.equals(rkCzry, wzRk.rkCzry) && Objects.equals(rkTg, wzRk.rkTg) && Objects.equals(rkBz, wzRk.rkBz);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, rkDh, rkType, rkSl, rkFs, rkCk, rkZt, rkCzry, rkTg, rkBz);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "WzRk{" +
                "id=" + id +
                ", rkDh='" + rkDh + '\'' +
                ", rkType=" + rkType +
                ", rkSl=" + rkSl +
                ", rkFs='" + rkFs + '\'' +
                ", rkCk='" + rkCk + '\'' +
                ", rkZt=" + rkZt +
                ", rkCzry='" + rkCzry + '\'' +
                ", rkTg=" + rkTg +
                ", rkTime=" + rkTime +
                ", rkBz='" + rkBz + '\'' +
                '}';
    }
}
