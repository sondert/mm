package com.ruoyi.wzrk.domain;

import com.ruoyi.common.annotation.Excel;

/**
 * @Description: TODO
 * @author: sonder
 * @date: 2021年12月04日 17:27
 */
public class RkVo {
    private Integer id;

    private String wzTp;

    private String wzName;

    private String wzGg;

    private String wzDw;

    private Integer num;

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWzTp() {
        return wzTp;
    }

    public void setWzTp(String wzTp) {
        this.wzTp = wzTp;
    }

    public String getWzName() {
        return wzName;
    }

    public void setWzName(String wzName) {
        this.wzName = wzName;
    }

    public String getWzGg() {
        return wzGg;
    }

    public void setWzGg(String wzGg) {
        this.wzGg = wzGg;
    }

    public String getWzDw() {
        return wzDw;
    }

    public void setWzDw(String wzDw) {
        this.wzDw = wzDw;
    }
}
