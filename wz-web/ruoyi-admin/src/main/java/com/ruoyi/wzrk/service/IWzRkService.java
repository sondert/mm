package com.ruoyi.wzrk.service;

import java.util.List;
import com.ruoyi.wzrk.domain.WzRk;

/**
 * 物资入库表Service接口
 * 
 * @author ruoyi
 * @date 2021-11-14
 */
public interface IWzRkService 
{
    /**
     * 查询物资入库表
     * 
     * @param id 物资入库表主键
     * @return 物资入库表
     */
    public WzRk selectWzRkById(Integer id);

    /**
     * 查询物资入库表列表
     * 
     * @param wzRk 物资入库表
     * @return 物资入库表集合
     */
    public List<WzRk> selectWzRkList(WzRk wzRk);

    /**
     * 新增物资入库表
     * 
     * @param wzRk 物资入库表
     * @return 结果
     */
    public int insertWzRk(WzRk wzRk);

    /**
     * 修改物资入库表
     * 
     * @param wzRk 物资入库表
     * @return 结果
     */
    public int updateWzRk(WzRk wzRk);

    /**
     * 批量删除物资入库表
     * 
     * @param ids 需要删除的物资入库表主键集合
     * @return 结果
     */
    public int deleteWzRkByIds(Integer[] ids);

    /**
     * 删除物资入库表信息
     * 
     * @param id 物资入库表主键
     * @return 结果
     */
    public int deleteWzRkById(Integer id);
}
