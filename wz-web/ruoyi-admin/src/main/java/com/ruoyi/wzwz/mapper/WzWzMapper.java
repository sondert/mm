package com.ruoyi.wzwz.mapper;

import java.util.List;
import com.ruoyi.wzwz.domain.WzWz;
/**
 * 物资资料Mapper接口
 * 
 * @author sonder
 * @date 2021-11-14
 */
public interface WzWzMapper 
{
    /**
     * 查询物资资料
     * 
     * @param id 物资资料主键
     * @return 物资资料
     */
    public WzWz selectWzWzById(Integer id);

    /**
     * 查询物资资料列表
     * 
     * @param wzWz 物资资料
     * @return 物资资料集合
     */
    public List<WzWz> selectWzWzList(WzWz wzWz);

    /**
     * 新增物资资料
     * 
     * @param wzWz 物资资料
     * @return 结果
     */
    public int insertWzWz(WzWz wzWz);

    /**
     * 修改物资资料
     * 
     * @param wzWz 物资资料
     * @return 结果
     */
    public int updateWzWz(WzWz wzWz);

    /**
     * 删除物资资料
     * 
     * @param id 物资资料主键
     * @return 结果
     */
    public int deleteWzWzById(Integer id);

    /**
     * 批量删除物资资料
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWzWzByIds(Integer[] ids);
}
