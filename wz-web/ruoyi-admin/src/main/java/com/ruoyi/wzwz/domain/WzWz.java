package com.ruoyi.wzwz.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物资资料对象 wz_wz
 * 
 * @author sonder
 * @date 2021-11-14
 */
public class WzWz extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 物资编号 */
    private Integer id;

    /** 物资图片 */
    @Excel(name = "物资图片")
    private String wzTp;

    /** 物资名称 */
    @Excel(name = "物资名称")
    private String wzName;

    /** 规格 */
    @Excel(name = "规格")
    private String wzGg;

    /** 单位 */
    @Excel(name = "单位")
    private String wzDw;

    /** 状态 */
    @Excel(name = "状态")
    private int wzZt;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date wzTime;

    /** 备注 */
    @Excel(name = "备注")
    private String wzBz;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setWzTp(String wzTp) 
    {
        this.wzTp = wzTp;
    }

    public String getWzTp() 
    {
        return wzTp;
    }
    public void setWzName(String wzName) 
    {
        this.wzName = wzName;
    }

    public String getWzName() 
    {
        return wzName;
    }
    public void setWzGg(String wzGg) 
    {
        this.wzGg = wzGg;
    }

    public String getWzGg() 
    {
        return wzGg;
    }
    public void setWzDw(String wzDw) 
    {
        this.wzDw = wzDw;
    }

    public String getWzDw() 
    {
        return wzDw;
    }
    public void setWzTime(Date wzTime) 
    {
        this.wzTime = wzTime;
    }

    public Date getWzTime() 
    {
        return wzTime;
    }
    public void setWzBz(String wzBz) 
    {
        this.wzBz = wzBz;
    }

    public String getWzBz() 
    {
        return wzBz;
    }

    public int getWzZt() {
        return wzZt;
    }

    public void setWzZt(int wzZt) {
        this.wzZt = wzZt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("wzTp", getWzTp())
            .append("wzName", getWzName())
            .append("wzGg", getWzGg())
            .append("wzDw", getWzDw())
            .append("wzTime", getWzTime())
            .append("wzBz", getWzBz())
            .append("wzZt", getWzZt())
            .toString();
    }
}
