package com.ruoyi.wzwz.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wzwz.mapper.WzWzMapper;
import com.ruoyi.wzwz.domain.WzWz;
import com.ruoyi.wzwz.service.IWzWzService;

/**
 * 物资资料Service业务层处理
 * 
 * @author sonder
 * @date 2021-11-14
 */
@Service
public class WzWzServiceImpl implements IWzWzService 
{
    @Autowired
    private WzWzMapper wzWzMapper;

    /**
     * 查询物资资料
     * 
     * @param id 物资资料主键
     * @return 物资资料
     */
    @Override
    public WzWz selectWzWzById(Integer id)
    {
        return wzWzMapper.selectWzWzById(id);
    }

    /**
     * 查询物资资料列表
     * 
     * @param wzWz 物资资料
     * @return 物资资料
     */
    @Override
    public List<WzWz> selectWzWzList(WzWz wzWz)
    {
        return wzWzMapper.selectWzWzList(wzWz);
    }

    /**
     * 新增物资资料
     * 
     * @param wzWz 物资资料
     * @return 结果
     */
    @Override
    public int insertWzWz(WzWz wzWz)
    {
        return wzWzMapper.insertWzWz(wzWz);
    }

    /**
     * 修改物资资料
     * 
     * @param wzWz 物资资料
     * @return 结果
     */
    @Override
    public int updateWzWz(WzWz wzWz)
    {
        return wzWzMapper.updateWzWz(wzWz);
    }

    /**
     * 批量删除物资资料
     * 
     * @param ids 需要删除的物资资料主键
     * @return 结果
     */
    @Override
    public int deleteWzWzByIds(Integer[] ids)
    {
        return wzWzMapper.deleteWzWzByIds(ids);
    }

    /**
     * 删除物资资料信息
     * 
     * @param id 物资资料主键
     * @return 结果
     */
    @Override
    public int deleteWzWzById(Integer id)
    {
        return wzWzMapper.deleteWzWzById(id);
    }
}
