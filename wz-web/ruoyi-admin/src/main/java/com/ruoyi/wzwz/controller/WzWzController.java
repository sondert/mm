package com.ruoyi.wzwz.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wzwz.domain.WzWz;
import com.ruoyi.wzwz.service.IWzWzService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物资资料Controller
 * 
 * @author sonder
 * @date 2021-11-14
 */
@RestController
@RequestMapping("/wzwz/wzwz")
public class WzWzController extends BaseController
{
    @Autowired
    private IWzWzService wzWzService;

//    @GetMapping(value = "/test")
//    public WzWz[] test(){
//        List<WzWz> list = wzWzService.selectWzWzList(new WzWz());
//        WzWz[] ans = new WzWz[list.size()];
//        int i=0;
//        for (WzWz wzWz : list) {
//            ans[i++] = wzWz;
//        }
//        return ans;
//    }

    /**
     * 查询物资资料列表
     */
    @PreAuthorize("@ss.hasPermi('wzwz:wzwz:list')")
    @GetMapping("/list")
    public TableDataInfo list(WzWz wzWz)
    {
        startPage();
        List<WzWz> list = wzWzService.selectWzWzList(wzWz);
        return getDataTable(list);
    }

    /**
     * 导出物资资料列表
     */
    @PreAuthorize("@ss.hasPermi('wzwz:wzwz:export')")
    @Log(title = "物资资料", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WzWz wzWz)
    {
        List<WzWz> list = wzWzService.selectWzWzList(wzWz);
        ExcelUtil<WzWz> util = new ExcelUtil<WzWz>(WzWz.class);
        return util.exportExcel(list, "物资资料数据");
    }

    /**
     * 获取物资资料详细信息
     */
    @PreAuthorize("@ss.hasPermi('wzwz:wzwz:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(wzWzService.selectWzWzById(id));
    }

    /**
     * 新增物资资料
     */
    @PreAuthorize("@ss.hasPermi('wzwz:wzwz:add')")
    @Log(title = "物资资料", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WzWz wzWz)
    {
        wzWz.setWzZt(1);
        return toAjax(wzWzService.insertWzWz(wzWz));
    }

    /**
     * 修改物资资料
     */
    @PreAuthorize("@ss.hasPermi('wzwz:wzwz:edit')")
    @Log(title = "物资资料", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WzWz wzWz)
    {
        return toAjax(wzWzService.updateWzWz(wzWz));
    }
    @PreAuthorize("@ss.hasPermi('wzwz:wzwz:pz')")
    @Log(title = "物资资料", businessType = BusinessType.UPDATE)
    @PutMapping(value = "/pz/{id}")
    public AjaxResult pz(@PathVariable("id") Integer id)
    {
        WzWz wzWz = wzWzService.selectWzWzById(id);
        wzWz.setWzZt(2);
        return toAjax(wzWzService.updateWzWz(wzWz));
    }

    /**
     * 删除物资资料
     */
    @PreAuthorize("@ss.hasPermi('wzwz:wzwz:remove')")
    @Log(title = "物资资料", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(wzWzService.deleteWzWzByIds(ids));
    }
}
