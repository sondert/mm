package com.ruoyi.wzcl.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wzcl.domain.WzCl;
import com.ruoyi.wzcl.service.IWzClService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 车辆信息表Controller
 * 
 * @author sonder
 * @date 2021-11-14
 */
@RestController
@RequestMapping("/wzcl/wzcl")
public class WzClController extends BaseController
{
    @Autowired
    private IWzClService wzClService;

    /**
     * 查询车辆信息表列表
     */
    @PreAuthorize("@ss.hasPermi('wzcl:wzcl:list')")
    @GetMapping("/list")
    public TableDataInfo list(WzCl wzCl)
    {
        startPage();
        List<WzCl> list = wzClService.selectWzClList(wzCl);
        return getDataTable(list);
    }

    /**
     * 导出车辆信息表列表
     */
    @PreAuthorize("@ss.hasPermi('wzcl:wzcl:export')")
    @Log(title = "车辆信息表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WzCl wzCl)
    {
        List<WzCl> list = wzClService.selectWzClList(wzCl);
        ExcelUtil<WzCl> util = new ExcelUtil<WzCl>(WzCl.class);
        return util.exportExcel(list, "车辆信息表数据");
    }

    /**
     * 获取车辆信息表详细信息
     */
    @PreAuthorize("@ss.hasPermi('wzcl:wzcl:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(wzClService.selectWzClById(id));
    }

    /**
     * 新增车辆信息表
     */
    @PreAuthorize("@ss.hasPermi('wzcl:wzcl:add')")
    @Log(title = "车辆信息表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WzCl wzCl)
    {
        return toAjax(wzClService.insertWzCl(wzCl));
    }

    /**
     * 修改车辆信息表
     */
    @PreAuthorize("@ss.hasPermi('wzcl:wzcl:edit')")
    @Log(title = "车辆信息表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WzCl wzCl)
    {
        return toAjax(wzClService.updateWzCl(wzCl));
    }

    /**
     * 删除车辆信息表
     */
    @PreAuthorize("@ss.hasPermi('wzcl:wzcl:remove')")
    @Log(title = "车辆信息表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(wzClService.deleteWzClByIds(ids));
    }
}
