package com.ruoyi.wzcl.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wzcl.mapper.WzClMapper;
import com.ruoyi.wzcl.domain.WzCl;
import com.ruoyi.wzcl.service.IWzClService;

/**
 * 车辆信息表Service业务层处理
 * 
 * @author sonder
 * @date 2021-11-14
 */
@Service
public class WzClServiceImpl implements IWzClService 
{
    @Autowired
    private WzClMapper wzClMapper;

    /**
     * 查询车辆信息表
     * 
     * @param id 车辆信息表主键
     * @return 车辆信息表
     */
    @Override
    public WzCl selectWzClById(Integer id)
    {
        return wzClMapper.selectWzClById(id);
    }

    /**
     * 查询车辆信息表列表
     * 
     * @param wzCl 车辆信息表
     * @return 车辆信息表
     */
    @Override
    public List<WzCl> selectWzClList(WzCl wzCl)
    {
        return wzClMapper.selectWzClList(wzCl);
    }

    /**
     * 新增车辆信息表
     * 
     * @param wzCl 车辆信息表
     * @return 结果
     */
    @Override
    public int insertWzCl(WzCl wzCl)
    {
        return wzClMapper.insertWzCl(wzCl);
    }

    /**
     * 修改车辆信息表
     * 
     * @param wzCl 车辆信息表
     * @return 结果
     */
    @Override
    public int updateWzCl(WzCl wzCl)
    {
        return wzClMapper.updateWzCl(wzCl);
    }

    /**
     * 批量删除车辆信息表
     * 
     * @param ids 需要删除的车辆信息表主键
     * @return 结果
     */
    @Override
    public int deleteWzClByIds(Integer[] ids)
    {
        return wzClMapper.deleteWzClByIds(ids);
    }

    /**
     * 删除车辆信息表信息
     * 
     * @param id 车辆信息表主键
     * @return 结果
     */
    @Override
    public int deleteWzClById(Integer id)
    {
        return wzClMapper.deleteWzClById(id);
    }
}
