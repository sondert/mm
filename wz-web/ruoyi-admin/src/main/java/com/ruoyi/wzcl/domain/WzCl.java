package com.ruoyi.wzcl.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 车辆信息表对象 wz_cl
 * 
 * @author sonder
 * @date 2021-11-14
 */
public class WzCl extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 车辆Id */
    private Integer id;

    /** 车牌号 */
    @Excel(name = "车牌号")
    private String clCph;

    /** 车辆种类 */
    @Excel(name = "车辆种类")
    private String clType;

    /** 车辆品牌 */
    @Excel(name = "车辆品牌")
    private String clPp;

    /** 联系人 */
    @Excel(name = "联系人")
    private String clLx;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String clLxfs;

    /** 备注 */
    @Excel(name = "备注")
    private String clBz;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setClCph(String clCph) 
    {
        this.clCph = clCph;
    }

    public String getClCph() 
    {
        return clCph;
    }
    public void setClType(String clType) 
    {
        this.clType = clType;
    }

    public String getClType() 
    {
        return clType;
    }
    public void setClPp(String clPp) 
    {
        this.clPp = clPp;
    }

    public String getClPp() 
    {
        return clPp;
    }
    public void setClLx(String clLx) 
    {
        this.clLx = clLx;
    }

    public String getClLx() 
    {
        return clLx;
    }
    public void setClLxfs(String clLxfs) 
    {
        this.clLxfs = clLxfs;
    }

    public String getClLxfs() 
    {
        return clLxfs;
    }
    public void setClBz(String clBz) 
    {
        this.clBz = clBz;
    }

    public String getClBz() 
    {
        return clBz;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("clCph", getClCph())
            .append("clType", getClType())
            .append("clPp", getClPp())
            .append("clLx", getClLx())
            .append("clLxfs", getClLxfs())
            .append("clBz", getClBz())
            .toString();
    }
}
