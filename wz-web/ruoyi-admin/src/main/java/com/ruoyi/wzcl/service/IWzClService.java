package com.ruoyi.wzcl.service;

import java.util.List;
import com.ruoyi.wzcl.domain.WzCl;

/**
 * 车辆信息表Service接口
 * 
 * @author sonder
 * @date 2021-11-14
 */
public interface IWzClService 
{
    /**
     * 查询车辆信息表
     * 
     * @param id 车辆信息表主键
     * @return 车辆信息表
     */
    public WzCl selectWzClById(Integer id);

    /**
     * 查询车辆信息表列表
     * 
     * @param wzCl 车辆信息表
     * @return 车辆信息表集合
     */
    public List<WzCl> selectWzClList(WzCl wzCl);

    /**
     * 新增车辆信息表
     * 
     * @param wzCl 车辆信息表
     * @return 结果
     */
    public int insertWzCl(WzCl wzCl);

    /**
     * 修改车辆信息表
     * 
     * @param wzCl 车辆信息表
     * @return 结果
     */
    public int updateWzCl(WzCl wzCl);

    /**
     * 批量删除车辆信息表
     * 
     * @param ids 需要删除的车辆信息表主键集合
     * @return 结果
     */
    public int deleteWzClByIds(Integer[] ids);

    /**
     * 删除车辆信息表信息
     * 
     * @param id 车辆信息表主键
     * @return 结果
     */
    public int deleteWzClById(Integer id);
}
