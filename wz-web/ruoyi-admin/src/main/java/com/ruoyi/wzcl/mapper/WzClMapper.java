package com.ruoyi.wzcl.mapper;

import java.util.List;
import com.ruoyi.wzcl.domain.WzCl;

/**
 * 车辆信息表Mapper接口
 * 
 * @author sonder
 * @date 2021-11-14
 */
public interface WzClMapper 
{
    /**
     * 查询车辆信息表
     * 
     * @param id 车辆信息表主键
     * @return 车辆信息表
     */
    public WzCl selectWzClById(Integer id);

    /**
     * 查询车辆信息表列表
     * 
     * @param wzCl 车辆信息表
     * @return 车辆信息表集合
     */
    public List<WzCl> selectWzClList(WzCl wzCl);

    /**
     * 新增车辆信息表
     * 
     * @param wzCl 车辆信息表
     * @return 结果
     */
    public int insertWzCl(WzCl wzCl);

    /**
     * 修改车辆信息表
     * 
     * @param wzCl 车辆信息表
     * @return 结果
     */
    public int updateWzCl(WzCl wzCl);

    /**
     * 删除车辆信息表
     * 
     * @param id 车辆信息表主键
     * @return 结果
     */
    public int deleteWzClById(Integer id);

    /**
     * 批量删除车辆信息表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWzClByIds(Integer[] ids);
}
