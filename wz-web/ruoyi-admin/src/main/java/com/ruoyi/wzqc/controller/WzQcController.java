package com.ruoyi.wzqc.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wzqc.domain.WzQc;
import com.ruoyi.wzqc.service.IWzQcService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物资流向Controller
 * 
 * @author sonder
 * @date 2021-11-14
 */
@RestController
@RequestMapping("/wzqc/wzqc")
public class WzQcController extends BaseController
{
    @Autowired
    private IWzQcService wzQcService;

    /**
     * 查询物资流向列表
     */
    @PreAuthorize("@ss.hasPermi('wzqc:wzqc:list')")
    @GetMapping("/list")
    public TableDataInfo list(WzQc wzQc)
    {
        startPage();
        List<WzQc> list = wzQcService.selectWzQcList(wzQc);
        return getDataTable(list);
    }

    /**
     * 导出物资流向列表
     */
    @PreAuthorize("@ss.hasPermi('wzqc:wzqc:export')")
    @Log(title = "物资流向", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WzQc wzQc)
    {
        List<WzQc> list = wzQcService.selectWzQcList(wzQc);
        ExcelUtil<WzQc> util = new ExcelUtil<WzQc>(WzQc.class);
        return util.exportExcel(list, "物资流向数据");
    }

    /**
     * 获取物资流向详细信息
     */
    @PreAuthorize("@ss.hasPermi('wzqc:wzqc:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(wzQcService.selectWzQcById(id));
    }

    /**
     * 新增物资流向
     */
    @PreAuthorize("@ss.hasPermi('wzqc:wzqc:add')")
    @Log(title = "物资流向", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WzQc wzQc)
    {
        return toAjax(wzQcService.insertWzQc(wzQc));
    }

    /**
     * 修改物资流向
     */
    @PreAuthorize("@ss.hasPermi('wzqc:wzqc:edit')")
    @Log(title = "物资流向", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WzQc wzQc)
    {
        return toAjax(wzQcService.updateWzQc(wzQc));
    }

    /**
     * 删除物资流向
     */
    @PreAuthorize("@ss.hasPermi('wzqc:wzqc:remove')")
    @Log(title = "物资流向", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(wzQcService.deleteWzQcByIds(ids));
    }
}
