package com.ruoyi.wzqc.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物资流向对象 wz_qc
 * 
 * @author sonder
 * @date 2021-11-14
 */
public class WzQc extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 去处id */
    private Integer id;

    /** 省份 */
    @Excel(name = "省份")
    private String qcSf;

    /** 市 */
    @Excel(name = "市")
    private String qcS;

    /** 区县 */
    @Excel(name = "区县")
    private String qcQx;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String qcDz;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date qcTime;

    /** 联系人 */
    @Excel(name = "联系人")
    private String qcLx;

    /** 电话 */
    @Excel(name = "电话")
    private String qcDh;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setQcSf(String qcSf) 
    {
        this.qcSf = qcSf;
    }

    public String getQcSf() 
    {
        return qcSf;
    }
    public void setQcS(String qcS) 
    {
        this.qcS = qcS;
    }

    public String getQcS() 
    {
        return qcS;
    }
    public void setQcQx(String qcQx) 
    {
        this.qcQx = qcQx;
    }

    public String getQcQx() 
    {
        return qcQx;
    }
    public void setQcDz(String qcDz) 
    {
        this.qcDz = qcDz;
    }

    public String getQcDz() 
    {
        return qcDz;
    }
    public void setQcTime(Date qcTime) 
    {
        this.qcTime = qcTime;
    }

    public Date getQcTime() 
    {
        return qcTime;
    }
    public void setQcLx(String qcLx) 
    {
        this.qcLx = qcLx;
    }

    public String getQcLx() 
    {
        return qcLx;
    }
    public void setQcDh(String qcDh) 
    {
        this.qcDh = qcDh;
    }

    public String getQcDh() 
    {
        return qcDh;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("qcSf", getQcSf())
            .append("qcS", getQcS())
            .append("qcQx", getQcQx())
            .append("qcDz", getQcDz())
            .append("qcTime", getQcTime())
            .append("qcLx", getQcLx())
            .append("qcDh", getQcDh())
            .toString();
    }
}
