package com.ruoyi.wzqc.service;

import java.util.List;
import com.ruoyi.wzqc.domain.WzQc;

/**
 * 物资流向Service接口
 * 
 * @author sonder
 * @date 2021-11-14
 */
public interface IWzQcService 
{
    /**
     * 查询物资流向
     * 
     * @param id 物资流向主键
     * @return 物资流向
     */
    public WzQc selectWzQcById(Integer id);

    /**
     * 查询物资流向列表
     * 
     * @param wzQc 物资流向
     * @return 物资流向集合
     */
    public List<WzQc> selectWzQcList(WzQc wzQc);

    /**
     * 新增物资流向
     * 
     * @param wzQc 物资流向
     * @return 结果
     */
    public int insertWzQc(WzQc wzQc);

    /**
     * 修改物资流向
     * 
     * @param wzQc 物资流向
     * @return 结果
     */
    public int updateWzQc(WzQc wzQc);

    /**
     * 批量删除物资流向
     * 
     * @param ids 需要删除的物资流向主键集合
     * @return 结果
     */
    public int deleteWzQcByIds(Integer[] ids);

    /**
     * 删除物资流向信息
     * 
     * @param id 物资流向主键
     * @return 结果
     */
    public int deleteWzQcById(Integer id);
}
