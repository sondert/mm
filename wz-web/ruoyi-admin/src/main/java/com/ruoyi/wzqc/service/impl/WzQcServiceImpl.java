package com.ruoyi.wzqc.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wzqc.mapper.WzQcMapper;
import com.ruoyi.wzqc.domain.WzQc;
import com.ruoyi.wzqc.service.IWzQcService;

/**
 * 物资流向Service业务层处理
 * 
 * @author sonder
 * @date 2021-11-14
 */
@Service
public class WzQcServiceImpl implements IWzQcService 
{
    @Autowired
    private WzQcMapper wzQcMapper;

    /**
     * 查询物资流向
     * 
     * @param id 物资流向主键
     * @return 物资流向
     */
    @Override
    public WzQc selectWzQcById(Integer id)
    {
        return wzQcMapper.selectWzQcById(id);
    }

    /**
     * 查询物资流向列表
     * 
     * @param wzQc 物资流向
     * @return 物资流向
     */
    @Override
    public List<WzQc> selectWzQcList(WzQc wzQc)
    {
        return wzQcMapper.selectWzQcList(wzQc);
    }

    /**
     * 新增物资流向
     * 
     * @param wzQc 物资流向
     * @return 结果
     */
    @Override
    public int insertWzQc(WzQc wzQc)
    {
        return wzQcMapper.insertWzQc(wzQc);
    }

    /**
     * 修改物资流向
     * 
     * @param wzQc 物资流向
     * @return 结果
     */
    @Override
    public int updateWzQc(WzQc wzQc)
    {
        return wzQcMapper.updateWzQc(wzQc);
    }

    /**
     * 批量删除物资流向
     * 
     * @param ids 需要删除的物资流向主键
     * @return 结果
     */
    @Override
    public int deleteWzQcByIds(Integer[] ids)
    {
        return wzQcMapper.deleteWzQcByIds(ids);
    }

    /**
     * 删除物资流向信息
     * 
     * @param id 物资流向主键
     * @return 结果
     */
    @Override
    public int deleteWzQcById(Integer id)
    {
        return wzQcMapper.deleteWzQcById(id);
    }
}
