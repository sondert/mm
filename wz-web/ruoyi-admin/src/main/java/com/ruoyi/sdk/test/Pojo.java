package com.ruoyi.sdk.test;

import org.springframework.security.core.parameters.P;

/**
 * @Description: TODO
 * @author: sonder
 * @date: 2021年11月29日 15:08
 */
public class Pojo {

    public static void main(String[] args) throws CloneNotSupportedException {
        Pojo pojo = new Pojo();
        Pojo pojo1 = (Pojo) pojo.clone();
    }
    public String name;
    public int age;

    @Override
    public String toString() {
        return "Pojo{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public Pojo(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public Pojo() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
