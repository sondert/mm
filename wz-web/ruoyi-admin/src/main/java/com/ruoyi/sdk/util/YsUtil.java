package com.ruoyi.sdk.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruoyi.sdk.po.YsHis;
import com.ruoyi.wzys.domain.WzYs;
import org.springframework.stereotype.Service;

/**
 * @Description: TODO
 * @author: sonder
 * @date: 2021年11月28日 15:39
 */
@Service
public class YsUtil {
    private InvokeUtil invokeUtil = new InvokeUtil();
    private static ObjectMapper mapper = new ObjectMapper();
    private String channelName = "ys";
    private String chainCodeName = "yscc";
    public void save(String UUID, WzYs wzYs) throws Exception{
        invokeUtil.invoke("save",new String[]{UUID,mapper.writeValueAsString(wzYs)},channelName,chainCodeName);
    }

    public WzYs query(String UUID) throws Exception{
        return mapper.readValue(invokeUtil.query(UUID,channelName,chainCodeName).get(200).toString(), WzYs.class);
    }


    public void delete(String UUID) throws Exception{
        invokeUtil.delete(UUID,channelName,chainCodeName);
    }

    public YsHis[] queryAll(String UUID) throws Exception{
        return mapper.readValue(invokeUtil.queryAll(UUID,channelName,chainCodeName).get(200).toString(),YsHis[].class);
    }

//    public static void main(String[] args) throws Exception{
//        YsUtil ysUtil = new YsUtil();
//        ysUtil.save("1256",new WzYs());
//        System.out.println(ysUtil.query("45456"));
//        ysUtil.delete("45456");
//        System.out.println(ysUtil.queryAll("45456"));
//    }
}
