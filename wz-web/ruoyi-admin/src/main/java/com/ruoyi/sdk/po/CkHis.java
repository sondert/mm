package com.ruoyi.sdk.po;

import com.ruoyi.wzff.domain.WzFf;

/**
 * @Description: TODO
 * @author: sonder
 * @date: 2021年11月29日 20:01
 */
public class CkHis {
    public String TID;
    public WzFf THistory;
    public String  TTime;
    public String IsDelete;

    @Override
    public String toString() {
        return "CkHis{" +
                "TID='" + TID + '\'' +
                ", THistory=" + THistory +
                ", TTime='" + TTime + '\'' +
                ", IsDelete='" + IsDelete + '\'' +
                '}';
    }

    public String getTID() {
        return TID;
    }

    public void setTID(String TID) {
        this.TID = TID;
    }

    public WzFf getTHistory() {
        return THistory;
    }

    public void setTHistory(WzFf THistory) {
        this.THistory = THistory;
    }

    public String getTTime() {
        return TTime;
    }

    public void setTTime(String TTime) {
        this.TTime = TTime;
    }

    public String getIsDelete() {
        return IsDelete;
    }

    public void setIsDelete(String isDelete) {
        IsDelete = isDelete;
    }
}
