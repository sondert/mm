package com.ruoyi.sdk.util;

import com.ruoyi.sdk.bean.SdkConfig;
import com.ruoyi.sdk.client.FabricClient;
import com.ruoyi.sdk.po.UserContext;
import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.exception.CryptoException;
import org.hyperledger.fabric.sdk.exception.InvalidArgumentException;
import org.hyperledger.fabric.sdk.exception.ProposalException;
import org.hyperledger.fabric.sdk.exception.TransactionException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

/**
 * @Description: TODO
 * @author: sonder
 * @date: 2021年11月28日 15:38
 */
public class ChannelUtil {
    /*
     * 创建通道
     */
    public void  channel1(String channelName,String txfilePath) throws Exception {

        UserContext userContext = new UserContext();
        userContext.setAffiliation("Org1");
        userContext.setMspId("Org1MSP");
        userContext.setAccount("李伟");
        userContext.setName("admin");
        Enrollment enrollment =  UserUtils.getEnrollment(SdkConfig.keyFolderPath1,SdkConfig.keyFileName1,SdkConfig.certFolderPath1,SdkConfig.certFileName1);
        userContext.setEnrollment(enrollment);
        FabricClient fabricClient = new FabricClient(userContext);
//        Channel channel =  fabricClient.createChannel("test",fabricClient.getOrderer("orderer.example.com","grpcs://orderer.example.com:7050",SdkConfig.tlsOrderFilePath),SdkConfig.txfilePath);
        Channel channel =  fabricClient.createChannel(channelName,fabricClient.getOrderer(SdkConfig.orderName,SdkConfig.orderGrpc,SdkConfig.tlsOrderFilePath),txfilePath);
//        Channel channel = fabricClient.getChannel(SdkConfig.channelName);

        channel.addOrderer(fabricClient.getOrderer(SdkConfig.orderName,SdkConfig.orderGrpc,SdkConfig.tlsOrderFilePath));
        channel.joinPeer(fabricClient.getPeer(SdkConfig.peer0Name1,SdkConfig.peer0Grpc1,SdkConfig.tlsPeerFilePath1));
        channel.initialize();

    }
    public void  channel2(String channelName) throws Exception {
        UserContext userContext = new UserContext();
        userContext.setAffiliation("Org2");
        userContext.setMspId("Org2MSP");
        userContext.setAccount("李伟");
        userContext.setName("admin");
        Enrollment enrollment =  UserUtils.getEnrollment(SdkConfig.keyFolderPath2,SdkConfig.keyFileName2,SdkConfig.certFolderPath2,SdkConfig.certFileName2);
        userContext.setEnrollment(enrollment);
        FabricClient fabricClient = new FabricClient(userContext);
//        Channel channel =  fabricClient.createChannel("test",fabricClient.getOrderer("orderer.example.com","grpcs://orderer.example.com:7050",SdkConfig.tlsOrderFilePath),SdkConfig.txfilePath);
//        Channel channel =  fabricClient.createChannel(SdkConfig.channelName,fabricClient.getOrderer(SdkConfig.orderName,SdkConfig.orderGrpc,SdkConfig.tlsOrderFilePath),SdkConfig.txfilePath);
        Channel channel = fabricClient.getChannel(channelName);

        channel.addOrderer(fabricClient.getOrderer(SdkConfig.orderName,SdkConfig.orderGrpc,SdkConfig.tlsOrderFilePath));

        channel.joinPeer(fabricClient.getPeer(SdkConfig.peer0Name2,SdkConfig.peer0Grpc2,SdkConfig.tlsPeerFilePath2));
        channel.initialize();

    }

    public  void channel(String channelName,String txfilePath) throws Exception{
//        channel1(channelName,txfilePath);
//        channel2(channelName);
    }

}
