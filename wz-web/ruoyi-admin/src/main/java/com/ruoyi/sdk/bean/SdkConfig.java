package com.ruoyi.sdk.bean;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @Description: TODO
 * @author: sonder
 * @date: 2021年11月28日 14:25
 */
@Component
@ConfigurationProperties(prefix = "sdk")
public class SdkConfig {
    /**
     * org1
     */
    public static String keyFolderPath1;
    public static String keyFileName1;
    public static String certFolderPath1;
    public static String certFileName1;
    public static String tlsPeerFilePath1;
    /**
     * org2
     */
    public static String keyFolderPath2;
    public static String keyFileName2;
    public static String certFolderPath2;
    public static String certFileName2;
    public static String tlsPeerFilePath2;

    /**
     * order
     */
    public static String tlsOrderFilePath;


    /**
     * order节点信息
     */
    public static String orderName;
    public static String orderGrpc;

    /**
     * peer节点信息
     */
    public static String peer0Name1;
    public static String peer0Grpc1;
    public static String peer1Name1;
    public static String peer1Grpc1;

    public static String peer0Name2;
    public static String peer0Grpc2;
    public static String peer1Name2;
    public static String peer1Grpc2;

    /**
     * 合约相关
     */
    public static String chainCodeLocation;
    public static String endorsementPolicy;
    public static String chainCodeName;
    public static String chainCodeVersion;
    public static String newVersion;
    public static String chainCodePath;

    /**
     * channel相关
     */
    public static String channelName;
    public static String txfilePath;

    public static String rkchainCodeName;
    public static String rktxfilePath;
    public static String rkendorsementPolicy;
    public static String rkchainCodePath;
    public static String rkchannelName;

    public static String ckchainCodeName;
    public static String cktxfilePath;
    public static String ckendorsementPolicy;
    public static String ckchainCodePath;
    public static String ckchannelName;

    public static String yschainCodeName;
    public static String ystxfilePath;
    public static String ysendorsementPolicy;
    public static String yschainCodePath;
    public static String yschannelName;

    static {
        Properties properties = new Properties();
        InputStream inputStream = Object.class.getResourceAsStream("/sdk.properties");
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        keyFolderPath1 = properties.get("keyFolderPath1").toString();
        keyFileName1 = properties.get("keyFileName1").toString();
        certFolderPath1 = properties.get("certFolderPath1").toString();
        certFileName1 = properties.get("certFileName1").toString();
        tlsPeerFilePath1 = properties.get("tlsPeerFilePath1").toString();

        keyFolderPath2 = properties.get("keyFolderPath2").toString();
        keyFileName2 = properties.get("keyFileName2").toString();
        certFolderPath2 = properties.get("certFolderPath2").toString();
        certFileName2 = properties.get("certFileName2").toString();
        tlsPeerFilePath2 = properties.get("tlsPeerFilePath2").toString();

        tlsOrderFilePath = properties.get("tlsOrderFilePath").toString();
        orderName = properties.get("orderName").toString();
        orderGrpc = properties.get("orderGrpc").toString();
        peer0Name1 = properties.get("peer0Name1").toString();
        peer0Grpc1 = properties.get("peer0Grpc1").toString();
        peer1Name1 = properties.get("peer1Name1").toString();
        peer1Grpc1 = properties.get("peer1Grpc1").toString();
        peer0Name2 = properties.get("peer0Name2").toString();
        peer0Grpc2 = properties.get("peer0Grpc2").toString();
        peer1Name2 = properties.get("peer1Name2").toString();
        peer1Grpc2 = properties.get("peer1Grpc2").toString();

        chainCodeLocation = properties.get("chainCodeLocation").toString();
        endorsementPolicy = properties.get("endorsementPolicy").toString();
        chainCodeName = properties.get("chainCodeName").toString();
        chainCodeVersion = properties.get("chainCodeVersion").toString();
        newVersion = properties.get("newVersion").toString();
        chainCodePath = properties.get("chainCodePath").toString();
        channelName = properties.get("channelName").toString();
        txfilePath = properties.get("txfilePath").toString();


        rkchainCodeName = properties.get("rkchainCodeName").toString();
        rktxfilePath = properties.get("rktxfilePath").toString();
        rkendorsementPolicy = properties.get("rkendorsementPolicy").toString();
        rkchainCodePath = properties.get("rkchainCodePath").toString();
        rkchannelName = properties.get("rkchannelName").toString();
        ckchainCodeName = properties.get("ckchainCodeName").toString();
        cktxfilePath = properties.get("cktxfilePath").toString();
        ckendorsementPolicy = properties.get("ckendorsementPolicy").toString();
        ckchainCodePath = properties.get("ckchainCodePath").toString();
        ckchannelName = properties.get("ckchannelName").toString();
        yschainCodeName = properties.get("yschainCodeName").toString();
        ystxfilePath = properties.get("ystxfilePath").toString();
        ysendorsementPolicy = properties.get("ysendorsementPolicy").toString();
        yschainCodePath = properties.get("yschainCodePath").toString();
        yschannelName = properties.get("yschannelName").toString();
    }


}
