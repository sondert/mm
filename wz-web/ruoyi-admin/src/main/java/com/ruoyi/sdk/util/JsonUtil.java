package com.ruoyi.sdk.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruoyi.wzrk.domain.WzRk;

/**
 * @Description: TODO
 * @author: sonder
 * @date: 2021年11月29日 16:28
 */
public class JsonUtil {
    private static ObjectMapper mapper = new ObjectMapper();

    public static String rktojson(WzRk wzRk) throws Exception{
        return mapper.writeValueAsString(wzRk);
    }

    public static WzRk jsontork(String jsonString) throws Exception{
        return mapper.readValue(jsonString,WzRk.class);
    }

    public static void main(String[] args) throws Exception{
//        System.out.println(rktojson(new WzRk()));
//        System.out.println(jsontork(rktojson(new WzRk())));
        String s1 = rktojson(new WzRk());String s2 = rktojson(new WzRk());
        WzRk[] wz = mapper.readValue("["+s1+","+s2+"]",WzRk[].class);
        System.out.println(wz[0]);
        System.out.println(wz[1]);
    }
}
