package com.ruoyi.sdk.po;


import com.ruoyi.wzys.domain.WzYs;

/**
 * @Description: TODO
 * @author: sonder
 * @date: 2021年11月29日 20:01
 */
public class YsHis {
    public String TID;
    public WzYs THistory;
    public String  TTime;
    public String IsDelete;

    @Override
    public String toString() {
        return "YsHis{" +
                "TID='" + TID + '\'' +
                ", THistory=" + THistory +
                ", TTime='" + TTime + '\'' +
                ", IsDelete='" + IsDelete + '\'' +
                '}';
    }

    public String getTID() {
        return TID;
    }

    public void setTID(String TID) {
        this.TID = TID;
    }

    public WzYs getTHistory() {
        return THistory;
    }

    public void setTHistory(WzYs THistory) {
        this.THistory = THistory;
    }

    public String getTTime() {
        return TTime;
    }

    public void setTTime(String TTime) {
        this.TTime = TTime;
    }

    public String getIsDelete() {
        return IsDelete;
    }

    public void setIsDelete(String isDelete) {
        IsDelete = isDelete;
    }
}
