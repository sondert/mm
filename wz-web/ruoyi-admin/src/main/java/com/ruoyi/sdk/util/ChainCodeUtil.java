package com.ruoyi.sdk.util;

import com.ruoyi.sdk.bean.SdkConfig;
import com.ruoyi.sdk.client.FabricClient;
import com.ruoyi.sdk.po.UserContext;
import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.Orderer;
import org.hyperledger.fabric.sdk.Peer;
import org.hyperledger.fabric.sdk.TransactionRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description: TODO
 * @author: sonder
 * @date: 2021年11月28日 15:38
 */
public class ChainCodeUtil {
    /*
     * 智能合约安装，初始化
     */
    public void azcode(String chainCodeName,String chainCodePath) throws Exception {
        UserContext userContext = new UserContext();
        userContext.setAffiliation("Org1");
        userContext.setMspId("Org1MSP");
        userContext.setAccount("李伟");
        userContext.setName("admin");
        Enrollment enrollment = UserUtils.getEnrollment(SdkConfig.keyFolderPath1, SdkConfig.keyFileName1, SdkConfig.certFolderPath1, SdkConfig.certFileName1);
        userContext.setEnrollment(enrollment);
        FabricClient fabricClient = new FabricClient(userContext);
        Peer peer0 = fabricClient.getPeer(SdkConfig.peer0Name1, SdkConfig.peer0Grpc1, SdkConfig.tlsPeerFilePath1);
        Peer peer1 = fabricClient.getPeer(SdkConfig.peer1Name1, SdkConfig.peer1Grpc1, SdkConfig.tlsPeerFilePath1);
        List<Peer> peers = new ArrayList<Peer>();
        peers.add(peer0);
        peers.add(peer1);
        fabricClient.installChaincode(TransactionRequest.Type.GO_LANG, chainCodeName, SdkConfig.chainCodeVersion, SdkConfig.chainCodeLocation, chainCodePath, peers);
    }

    public void azcode2(String chainCodeName,String chainCodePath) throws Exception {
        UserContext userContext = new UserContext();
        userContext.setAffiliation("Org1");
        userContext.setMspId("Org1MSP");
        userContext.setAccount("李伟");
        userContext.setName("admin");
        Enrollment enrollment = UserUtils.getEnrollment(SdkConfig.keyFolderPath1, SdkConfig.keyFileName1, SdkConfig.certFolderPath1, SdkConfig.certFileName1);
        userContext.setEnrollment(enrollment);
        FabricClient fabricClient = new FabricClient(userContext);
        Peer peer0 = fabricClient.getPeer(SdkConfig.peer0Name1, SdkConfig.peer0Grpc1, SdkConfig.tlsPeerFilePath1);
        Peer peer1 = fabricClient.getPeer(SdkConfig.peer1Name1, SdkConfig.peer1Grpc1, SdkConfig.tlsPeerFilePath1);
        List<Peer> peers = new ArrayList<Peer>();
        peers.add(peer0);
        peers.add(peer1);
        fabricClient.installChaincode(TransactionRequest.Type.GO_LANG, chainCodeName, SdkConfig.newVersion, SdkConfig.chainCodeLocation, chainCodePath, peers);
    }

    public void azcode3(String chainCodeName,String chainCodePath) throws Exception {
        UserContext userContext = new UserContext();
        userContext.setAffiliation("Org2");
        userContext.setMspId("Org2MSP");
        userContext.setAccount("李伟");
        userContext.setName("admin");
        Enrollment enrollment = UserUtils.getEnrollment(SdkConfig.keyFolderPath2, SdkConfig.keyFileName2, SdkConfig.certFolderPath2, SdkConfig.certFileName2);
        userContext.setEnrollment(enrollment);
        FabricClient fabricClient = new FabricClient(userContext);
        Peer peer0 = fabricClient.getPeer(SdkConfig.peer0Name2, SdkConfig.peer0Grpc2, SdkConfig.tlsPeerFilePath2);
        Peer peer1 = fabricClient.getPeer(SdkConfig.peer1Name2, SdkConfig.peer1Grpc2, SdkConfig.tlsPeerFilePath2);
        List<Peer> peers = new ArrayList<Peer>();
        peers.add(peer0);
        peers.add(peer1);
        fabricClient.installChaincode(TransactionRequest.Type.GO_LANG, chainCodeName, SdkConfig.newVersion, SdkConfig.chainCodeLocation, chainCodePath, peers);
    }

    /*
     * 智能合约实例化
     */
    public void slhcode(String channelName,String chainCodeName) throws Exception {
        UserContext userContext = new UserContext();
        userContext.setAffiliation("Org1");
        userContext.setMspId("Org1MSP");
        userContext.setAccount("李伟");
        userContext.setName("admin");
        Enrollment enrollment = UserUtils.getEnrollment(SdkConfig.keyFolderPath1, SdkConfig.keyFileName1, SdkConfig.certFolderPath1, SdkConfig.certFileName1);
        userContext.setEnrollment(enrollment);
        FabricClient fabricClient = new FabricClient(userContext);
        Peer peer = fabricClient.getPeer(SdkConfig.peer0Name1, SdkConfig.peer0Grpc1, SdkConfig.tlsPeerFilePath1);
        Orderer order = fabricClient.getOrderer(SdkConfig.orderName,SdkConfig.orderGrpc, SdkConfig.tlsOrderFilePath);
        String initArgs[] = {""};
        fabricClient.initChaincode(channelName, TransactionRequest.Type.GO_LANG, chainCodeName, SdkConfig.chainCodeVersion, order, peer, "init", initArgs);
    }

    /*
     * 智能合约升级
     */
    public void sjcode(String endorsementPolicy,String channelName,String chainCodeName) throws Exception {
        UserContext userContext = new UserContext();
        userContext.setAffiliation("Org1");
        userContext.setMspId("Org1MSP");
        userContext.setAccount("李伟");
        userContext.setName("admin");
        Enrollment enrollment = UserUtils.getEnrollment(SdkConfig.keyFolderPath1, SdkConfig.keyFileName1, SdkConfig.certFolderPath1, SdkConfig.certFileName1);
        userContext.setEnrollment(enrollment);
        FabricClient fabricClient = new FabricClient(userContext);
        Peer peer = fabricClient.getPeer(SdkConfig.peer0Name1,SdkConfig.peer0Grpc1, SdkConfig.tlsPeerFilePath1);
        Orderer order = fabricClient.getOrderer(SdkConfig.orderName,SdkConfig.orderGrpc, SdkConfig.tlsOrderFilePath);
        String initArgs[] = {""};
        fabricClient.upgradeChaincode(endorsementPolicy,channelName, TransactionRequest.Type.GO_LANG, chainCodeName, SdkConfig.newVersion, order, peer, "init", initArgs);
    }


//    public void sj ()throws Exception{
//        azcode2();
//        sjcode();
//        azcode3();
//    }

    public void az(String chainCodeName,String chainCodePath,String channelName,String endorsementPolicy) throws Exception{
//        azcode(chainCodeName,chainCodePath);
//        slhcode(channelName,chainCodeName);
//        azcode2(chainCodeName,chainCodePath);
//        sjcode(endorsementPolicy,channelName,chainCodeName);
//        azcode3(chainCodeName,chainCodePath);
    }
}