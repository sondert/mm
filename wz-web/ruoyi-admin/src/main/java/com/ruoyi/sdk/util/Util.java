package com.ruoyi.sdk.util;


import com.ruoyi.sdk.bean.SdkConfig;

/**
 * @Description: TODO
 * @author: sonder
 * @date: 2021年11月30日 10:10
 */
public class Util {
    private static ChannelUtil channelUtil= new ChannelUtil();
    private static ChainCodeUtil chainCodeUtil = new ChainCodeUtil();
    public static void main(String[] args) throws Exception{
//        channelUtil.channel(SdkConfig.rkchannelName,SdkConfig.rktxfilePath);
//        channelUtil.channel(SdkConfig.ckchannelName,SdkConfig.cktxfilePath);
//        channelUtil.channel(SdkConfig.yschannelName,SdkConfig.ystxfilePath);
        chainCodeUtil.az(SdkConfig.rkchainCodeName,SdkConfig.rkchainCodePath,SdkConfig.rkchannelName,SdkConfig.rkendorsementPolicy);
        chainCodeUtil.az(SdkConfig.ckchainCodeName,SdkConfig.ckchainCodePath,SdkConfig.ckchannelName,SdkConfig.ckendorsementPolicy);
        chainCodeUtil.az(SdkConfig.yschainCodeName,SdkConfig.yschainCodePath,SdkConfig.yschannelName,SdkConfig.ysendorsementPolicy);
    }
}
