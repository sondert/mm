package com.ruoyi.sdk.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruoyi.sdk.po.RKHis;
import com.ruoyi.wzrk.domain.WzRk;
import org.springframework.stereotype.Service;

/**
 * @Description: TODO
 * @author: sonder
 * @date: 2021年11月28日 15:39
 */
@Service
public class RkUtil {
    private InvokeUtil invokeUtil = new InvokeUtil();
    private static ObjectMapper mapper = new ObjectMapper();
    private String channelName = "rk";
    private String chainCodeName = "rkcc";
    public void save(String UUID,WzRk wzRk) throws Exception{
        invokeUtil.invoke("save",new String[]{UUID,mapper.writeValueAsString(wzRk)},channelName,chainCodeName);
    }

    public WzRk query(String UUID) throws Exception{
        return mapper.readValue(invokeUtil.query(UUID,channelName,chainCodeName).get(200).toString(), WzRk.class);
    }


    public void delete(String UUID) throws Exception{
        invokeUtil.delete(UUID,channelName,chainCodeName);
    }

    public RKHis[] queryAll(String UUID) throws Exception{
        return mapper.readValue(invokeUtil.queryAll(UUID,channelName,chainCodeName).get(200).toString(),RKHis[].class);
    }

//    public static void main(String[] args) throws Exception{
//        RkUtil rkUtil = new RkUtil();
//        rkUtil.save("45456",new WzRk());
//        System.out.println(rkUtil.query("45456"));
//        rkUtil.delete("45456");
//        System.out.println(rkUtil.queryAll("45456"));
//    }
}
