package com.ruoyi.sdk.po;

import com.ruoyi.wzrk.domain.WzRk;

/**
 * @Description: TODO
 * @author: sonder
 * @date: 2021年11月29日 17:09
 */
public class RKHis {
    public String TID;
    public WzRk THistory;
    public String  TTime;
    public String IsDelete;

    public String getTID() {
        return TID;
    }

    public void setTID(String TID) {
        this.TID = TID;
    }

    public WzRk getTHistory() {
        return THistory;
    }

    public void setTHistory(WzRk THistory) {
        this.THistory = THistory;
    }

    public String getTTime() {
        return TTime;
    }

    public void setTTime(String TTime) {
        this.TTime = TTime;
    }

    public String getIsDelete() {
        return IsDelete;
    }

    public void setIsDelete(String isDelete) {
        IsDelete = isDelete;
    }

    @Override
    public String toString() {
        return "History{" +
                "TID='" + TID + '\'' +
                ", THistory=" + THistory +
                ", TTime='" + TTime + '\'' +
                ", IsDelete='" + IsDelete + '\'' +
                '}';
    }
}
