package com.ruoyi.sdk.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruoyi.sdk.po.CkHis;
import com.ruoyi.wzff.domain.WzFf;
import org.springframework.stereotype.Service;


/**
 * @Description: TODO
 * @author: sonder
 * @date: 2021年11月28日 15:39
 */
@Service
public class CkUtil {
    private InvokeUtil invokeUtil = new InvokeUtil();
    private static ObjectMapper mapper = new ObjectMapper();
    private String channelName = "ck";
    private String chainCodeName = "ckcc";
    public void save(String UUID, WzFf wzFf) throws Exception{
        invokeUtil.invoke("save",new String[]{UUID,mapper.writeValueAsString(wzFf)},channelName,chainCodeName);
    }

    public WzFf query(String UUID) throws Exception{
        return mapper.readValue(invokeUtil.query(UUID,channelName,chainCodeName).get(200).toString(), WzFf.class);
    }


    public void delete(String UUID) throws Exception{
        invokeUtil.delete(UUID,channelName,chainCodeName);
    }

    public CkHis[] queryAll(String UUID) throws Exception{
        return mapper.readValue(invokeUtil.queryAll(UUID,channelName,chainCodeName).get(200).toString(),CkHis[].class);
    }

//    public static void main(String[] args) throws Exception{
//        CkUtil ckUtil = new CkUtil();
//        ckUtil.save("45456",new WzCk());
//        System.out.println(ckUtil.query("45456"));
//        ckUtil.delete("45456");

//        System.out.println(ckUtil.queryAll("45456"));
//    }
}
