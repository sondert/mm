package com.ruoyi.sdk.util;

import com.ruoyi.sdk.bean.SdkConfig;
import com.ruoyi.sdk.client.FabricClient;
import com.ruoyi.sdk.po.UserContext;
import com.ruoyi.wzrk.domain.WzRk;
import org.hyperledger.fabric.sdk.Enrollment;
import org.hyperledger.fabric.sdk.Orderer;
import org.hyperledger.fabric.sdk.Peer;
import org.hyperledger.fabric.sdk.TransactionRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Description: TODO
 * @author: sonder
 * @date: 2021年11月29日 16:26
 */
public class InvokeUtil {
    /*
     * invoke合约
     */
    public void invokecode() throws Exception {
        UserContext userContext = new UserContext();
        userContext.setAffiliation("Org1");
        userContext.setMspId("Org1MSP");
        userContext.setAccount("李伟");
        userContext.setName("admin");
        Enrollment enrollment = UserUtils.getEnrollment(SdkConfig.keyFolderPath1, SdkConfig.keyFileName1, SdkConfig.certFolderPath1, SdkConfig.certFileName1);
        userContext.setEnrollment(enrollment);
        FabricClient fabricClient = new FabricClient(userContext);
        Peer peer0 = fabricClient.getPeer(SdkConfig.peer0Name1,SdkConfig.peer0Grpc1, SdkConfig.tlsPeerFilePath1);
        Peer peer1 = fabricClient.getPeer(SdkConfig.peer0Name2,SdkConfig.peer0Grpc2, SdkConfig.tlsPeerFilePath2);
        List<Peer> peers = new ArrayList<>();
        peers.add(peer0);
        peers.add(peer1);
        Orderer order = fabricClient.getOrderer("orderer.example.com", "grpcs://orderer.example.com:7050", SdkConfig.tlsOrderFilePath);
//        String initArgs[] = {"110114", "{\"name\":\"zhangsan\",\"identity\":\"110114\",\"mobile\":\"18910012222\"}"};
        WzRk rk = new WzRk();
        rk.setId(1);
        rk.setRkBz("jfdlas");
//        String initArgs[] = {"110114", JsonUtil.rktojson(new WzRk())};
        String initArgs[] = {"110114", JsonUtil.rktojson(rk)};
        fabricClient.invoke(SdkConfig.channelName, TransactionRequest.Type.GO_LANG, SdkConfig.chainCodeName, order, peers, "save", initArgs);
    }
    public void delete(String UUID,String channelName,String chainCodeName) throws Exception {
        UserContext userContext = new UserContext();
        userContext.setAffiliation("Org1");
        userContext.setMspId("Org1MSP");
        userContext.setAccount("李伟");
        userContext.setName("admin");
        Enrollment enrollment = UserUtils.getEnrollment(SdkConfig.keyFolderPath1, SdkConfig.keyFileName1, SdkConfig.certFolderPath1, SdkConfig.certFileName1);
        userContext.setEnrollment(enrollment);
        FabricClient fabricClient = new FabricClient(userContext);
        Peer peer0 = fabricClient.getPeer(SdkConfig.peer0Name1,SdkConfig.peer0Grpc1, SdkConfig.tlsPeerFilePath1);
        Peer peer1 = fabricClient.getPeer(SdkConfig.peer0Name2,SdkConfig.peer0Grpc2, SdkConfig.tlsPeerFilePath2);
        List<Peer> peers = new ArrayList<>();
        peers.add(peer0);
        peers.add(peer1);
        Orderer order = fabricClient.getOrderer(SdkConfig.orderName, SdkConfig.orderGrpc, SdkConfig.tlsOrderFilePath);
        fabricClient.invoke(channelName, TransactionRequest.Type.GO_LANG, chainCodeName, order, peers, "delete", new String[]{UUID});
    }
    public void invoke(String funcName,String initArgs[],String channelName,String chainCodeName) throws Exception {
        UserContext userContext = new UserContext();
        userContext.setAffiliation("Org1");
        userContext.setMspId("Org1MSP");
        userContext.setAccount("李伟");
        userContext.setName("admin");
        Enrollment enrollment = UserUtils.getEnrollment(SdkConfig.keyFolderPath1, SdkConfig.keyFileName1, SdkConfig.certFolderPath1, SdkConfig.certFileName1);
        userContext.setEnrollment(enrollment);
        FabricClient fabricClient = new FabricClient(userContext);
        Peer peer0 = fabricClient.getPeer(SdkConfig.peer0Name1,SdkConfig.peer0Grpc1, SdkConfig.tlsPeerFilePath1);
        Peer peer1 = fabricClient.getPeer(SdkConfig.peer0Name2,SdkConfig.peer0Grpc2, SdkConfig.tlsPeerFilePath2);
        List<Peer> peers = new ArrayList<>();
        peers.add(peer0);
        peers.add(peer1);
        Orderer order = fabricClient.getOrderer(SdkConfig.orderName, SdkConfig.orderGrpc, SdkConfig.tlsOrderFilePath);
        fabricClient.invoke(channelName, TransactionRequest.Type.GO_LANG, chainCodeName, order, peers, funcName, initArgs);
    }

    /*
     * query合约
     */
    public Map querycode() throws Exception {
        UserContext userContext = new UserContext();
        userContext.setAffiliation("Org1");
        userContext.setMspId("Org1MSP");
        userContext.setAccount("李伟");
        userContext.setName("admin");
        Enrollment enrollment = UserUtils.getEnrollment(SdkConfig.keyFolderPath1, SdkConfig.keyFileName1, SdkConfig.certFolderPath1, SdkConfig.certFileName1);
        userContext.setEnrollment(enrollment);
        FabricClient fabricClient = new FabricClient(userContext);
        Peer peer0 = fabricClient.getPeer(SdkConfig.peer0Name1,SdkConfig.peer0Grpc1, SdkConfig.tlsPeerFilePath1);
        Peer peer1 = fabricClient.getPeer(SdkConfig.peer0Name2,SdkConfig.peer0Grpc2, SdkConfig.tlsPeerFilePath2);
        List<Peer> peers = new ArrayList<>();
        peers.add(peer0);
        peers.add(peer1);
        String initArgs[] = {"110114"};
        Map map = fabricClient.queryChaincode(peers, SdkConfig.channelName, TransactionRequest.Type.GO_LANG, SdkConfig.chainCodeName, "query", initArgs);
        System.out.println(map);
        return map;
    }
    public Map query(String UUID,String channelName,String chainCodeName) throws Exception {
        UserContext userContext = new UserContext();
        userContext.setAffiliation("Org1");
        userContext.setMspId("Org1MSP");
        userContext.setAccount("李伟");
        userContext.setName("admin");
        Enrollment enrollment = UserUtils.getEnrollment(SdkConfig.keyFolderPath1, SdkConfig.keyFileName1, SdkConfig.certFolderPath1, SdkConfig.certFileName1);
        userContext.setEnrollment(enrollment);
        FabricClient fabricClient = new FabricClient(userContext);
        Peer peer0 = fabricClient.getPeer(SdkConfig.peer0Name1,SdkConfig.peer0Grpc1, SdkConfig.tlsPeerFilePath1);
        Peer peer1 = fabricClient.getPeer(SdkConfig.peer0Name2,SdkConfig.peer0Grpc2, SdkConfig.tlsPeerFilePath2);
        List<Peer> peers = new ArrayList<>();
        peers.add(peer0);
        peers.add(peer1);
        Map map = fabricClient.queryChaincode(peers, channelName, TransactionRequest.Type.GO_LANG, chainCodeName, "query", new String[]{UUID});
        System.out.println(map);
        return map;
    }
    public Map queryAll(String UUID,String channelName,String chainCodeName) throws Exception {
        UserContext userContext = new UserContext();
        userContext.setAffiliation("Org1");
        userContext.setMspId("Org1MSP");
        userContext.setAccount("李伟");
        userContext.setName("admin");
        Enrollment enrollment = UserUtils.getEnrollment(SdkConfig.keyFolderPath1, SdkConfig.keyFileName1, SdkConfig.certFolderPath1, SdkConfig.certFileName1);
        userContext.setEnrollment(enrollment);
        FabricClient fabricClient = new FabricClient(userContext);
        Peer peer0 = fabricClient.getPeer(SdkConfig.peer0Name1,SdkConfig.peer0Grpc1, SdkConfig.tlsPeerFilePath1);
        Peer peer1 = fabricClient.getPeer(SdkConfig.peer0Name2,SdkConfig.peer0Grpc2, SdkConfig.tlsPeerFilePath2);
        List<Peer> peers = new ArrayList<>();
        peers.add(peer0);
        peers.add(peer1);
        Map map = fabricClient.queryChaincode(peers, channelName, TransactionRequest.Type.GO_LANG, chainCodeName, "queryAll", new String[]{UUID});
        System.out.println(map);
        return map;
    }
}
