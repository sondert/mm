package com.ruoyi.sdk.test;

import com.ruoyi.sdk.client.FabricClient;
import com.ruoyi.sdk.po.UserContext;
import com.ruoyi.sdk.util.UserUtils;
import org.hyperledger.fabric.sdk.Channel;
import org.hyperledger.fabric.sdk.Enrollment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SdkMain {

    private static final Logger log = LoggerFactory.getLogger(SdkMain.class);

    private static final String keyFolderPath1 = "C:\\Users\\1\\Desktop\\javake\\me\\testfabric\\src\\main\\resources\\crypto-config\\peerOrganizations\\org1.example.com\\users\\Admin@org1.example.com\\msp\\keystore";
    private static final String keyFileName1= "1ffa0ce5a6ea1df613724ca257c09ccc86ffe0a3a3113eb6f9cde41d0f4cde1a_sk";
    private static final String certFoldePath1= "C:\\Users\\1\\Desktop\\javake\\me\\testfabric\\src\\main\\resources\\crypto-config\\peerOrganizations\\org1.example.com\\users\\Admin@org1.example.com\\msp\\admincerts";
    private static final String certFileName1= "Admin@org1.example.com-cert.pem";

    private static final String keyFolderPath2 = "C:\\Users\\1\\Desktop\\javake\\me\\testfabric\\src\\main\\resources\\crypto-config\\peerOrganizations\\org2.example.com\\users\\Admin@org2.example.com\\msp\\keystore";
    private static final String keyFileName2= "412b43c4a0af37814d7e913ecc48e3fe7acabc258a931e0722b795ca4f06f929_sk";
    private static final String certFoldePath2= "C:\\Users\\1\\Desktop\\javake\\me\\testfabric\\src\\main\\resources\\crypto-config\\peerOrganizations\\org2.example.com\\users\\Admin@org2.example.com\\msp\\admincerts";
    private static final String certFileName2= "Admin@org2.example.com-cert.pem";

    private static  final String tlsOrderFilePath = "C:\\Users\\1\\Desktop\\javake\\me\\testfabric\\src\\main\\resources\\crypto-config\\ordererOrganizations\\example.com\\tlsca\\tlsca.example.com-cert.pem";
    private static final String txfilePath = "C:\\Users\\1\\Desktop\\javake\\me\\testfabric\\src\\main\\resources\\test.tx";
    private static  final String tlsPeerFilePath = "C:\\Users\\1\\Desktop\\javake\\me\\testfabric\\src\\main\\resources\\crypto-config\\peerOrganizations\\org1.example.com\\peers\\peer0.org1.example.com\\msp\\tlscacerts\\tlsca.org1.example.com-cert.pem";
    //private static  final String tlsPeerFilePathAddtion = "C:\\Users\\1\\Desktop\\javake\\me\\testfabric\\src\\main\\resources\\crypto-config\\peerOrganizations\\org2.example.com\\tlsca\\tlsca.org2.example.com-cert.pem";
    private static  final String tlsPeerFilePathAddtion = "C:\\Users\\1\\Desktop\\javake\\me\\testfabric\\src\\main\\resources\\crypto-config\\peerOrganizations\\org2.example.com\\users\\Admin@org2.example.com\\msp\\tlscacerts\\tlsca.org2.example.com-cert.pem";
//   public static void main(String[] args) throws Exception {
//
//        UserContext userContext = new UserContext();
//        userContext.setAffiliation("Org1");
//        userContext.setMspId("Org1MSP");
//        userContext.setAccount("李伟");
//        userContext.setName("admin");
//        Enrollment enrollment =  UserUtils.getEnrollment(keyFolderPath1,keyFileName1,certFoldePath1,certFileName1);
//        userContext.setEnrollment(enrollment);
//        FabricClient fabricClient = new FabricClient(userContext);
//        Channel channel =  fabricClient.createChannel("test",fabricClient.getOrderer("orderer.example.com","grpcs://orderer.example.com:7050",tlsOrderFilePath),txfilePath);
//        //Channel channel = fabricClient.getChannel("test");
//        channel.addOrderer(fabricClient.getOrderer("orderer.example.com","grpcs://orderer.example.com:7050",tlsOrderFilePath));
//        channel.joinPeer(fabricClient.getPeer("peer0.org1.example.com","grpcs://peer0.org1.example.com:7051",tlsPeerFilePath));
//        channel.initialize();
//
//    }

    //安装合约
//    public static void main(String[] args) throws InvalidKeySpecException, NoSuchAlgorithmException, CryptoException, IOException, IllegalAccessException, InvalidArgumentException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, org.hyperledger.fabric.sdk.exception.CryptoException, TransactionException, ProposalException, org.bouncycastle.crypto.CryptoException {
//
//        UserContext userContext = new UserContext();
//        userContext.setAffiliation("Org2");
//        userContext.setMspId("Org2MSP");
//        userContext.setAccount("李伟");
//        userContext.setName("admin");
//        Enrollment enrollment =  UserUtils.getEnrollment(keyFolderPath2,keyFileName2,certFoldePath2,certFileName2);
//        userContext.setEnrollment(enrollment);
//        FabricClient fabricClient = new FabricClient(userContext);
//        Peer peer0 = fabricClient.getPeer("peer0.org2.example.com","grpcs://peer0.org2.example.com:9051",tlsPeerFilePathAddtion);
//        Peer peer1 = fabricClient.getPeer("peer1.org2.example.com","grpcs://peer1.org2.example.com:10051",tlsPeerFilePathAddtion);
//        List<Peer> peers = new ArrayList<Peer>();
//        peers.add(peer0);
//        peers.add(peer1);
//        fabricClient.installChaincode(TransactionRequest.Type.GO_LANG,"basicinfo","2.0","E:\\chaincode","basicinfo",peers);
//    }

//   //合约实例化
//    public static void main(String[] args) throws InvalidKeySpecException, NoSuchAlgorithmException, CryptoException, IOException, IllegalAccessException, InvalidArgumentException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, org.hyperledger.fabric.sdk.exception.CryptoException, ProposalException, TransactionException, org.bouncycastle.crypto.CryptoException {
//        UserContext userContext = new UserContext();
//        userContext.setAffiliation("Org1");
//        userContext.setMspId("Org1MSP");
//        userContext.setAccount("李伟");
//        userContext.setName("admin");
//        Enrollment enrollment =  UserUtils.getEnrollment(keyFolderPath1,keyFileName1,certFoldePath1,certFileName1);
//        userContext.setEnrollment(enrollment);
//        FabricClient fabricClient = new FabricClient(userContext);
//        Peer peer = fabricClient.getPeer("peer0.org1.example.com","grpcs://peer0.org1.example.com:7051",tlsPeerFilePath);
//        Orderer order = fabricClient.getOrderer("orderer.example.com","grpcs://orderer.example.com:7050",tlsOrderFilePath);
//        String initArgs[] = {""};
//        fabricClient.initChaincode("mychannel", TransactionRequest.Type.GO_LANG,"basicinfo","1.0",order,peer,"init",initArgs);
//    }

   //合约升级
//    public static void main(String[] args) throws InvalidKeySpecException, NoSuchAlgorithmException, CryptoException, IOException, IllegalAccessException, InvalidArgumentException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, org.hyperledger.fabric.sdk.exception.CryptoException, ProposalException, TransactionException, ChaincodeEndorsementPolicyParseException, org.bouncycastle.crypto.CryptoException {
//        UserContext userContext = new UserContext();
//        userContext.setAffiliation("Org1");
//        userContext.setMspId("Org1MSP");
//        userContext.setAccount("李伟");
//        userContext.setName("admin");
//        Enrollment enrollment =  UserUtils.getEnrollment(keyFolderPath1,keyFileName1,certFoldePath1,certFileName1);
//        userContext.setEnrollment(enrollment);
//        FabricClient fabricClient = new FabricClient(userContext);
//        Peer peer = fabricClient.getPeer("peer0.org1.example.com","grpcs://peer0.org1.example.com:7051",tlsPeerFilePath);
//        Orderer order = fabricClient.getOrderer("orderer.example.com","grpcs://orderer.example.com:7050",tlsOrderFilePath);
//        String initArgs[] = {""};
//        fabricClient.upgradeChaincode("mychannel", TransactionRequest.Type.GO_LANG,"basicinfo","2.0",order,peer,"init",initArgs);
//    }

    //invoke 合约
//     public static void main(String[] args) throws InvalidKeySpecException, NoSuchAlgorithmException, CryptoException, IOException, IllegalAccessException, InvalidArgumentException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, org.hyperledger.fabric.sdk.exception.CryptoException, ProposalException, TransactionException, ChaincodeEndorsementPolicyParseException, org.bouncycastle.crypto.CryptoException {
//        UserContext userContext = new UserContext();
//        userContext.setAffiliation("Org1");
//        userContext.setMspId("Org1MSP");
//        userContext.setAccount("李伟");
//        userContext.setName("admin");
//        Enrollment enrollment =  UserUtils.getEnrollment(keyFolderPath1,keyFileName1,certFoldePath1,certFileName1);
//        userContext.setEnrollment(enrollment);
//        FabricClient fabricClient = new FabricClient(userContext);
//        Peer peer0 = fabricClient.getPeer("peer0.org1.example.com","grpcs://peer0.org1.example.com:7051",tlsPeerFilePath);
//        Peer peer1 = fabricClient.getPeer("peer0.org2.example.com","grpcs://peer0.org2.example.com:9051",tlsPeerFilePathAddtion);
//        List<Peer> peers = new ArrayList<>();
//        peers.add(peer0);
//        peers.add(peer1);
//        Orderer order = fabricClient.getOrderer("orderer.example.com","grpcs://orderer.example.com:7050",tlsOrderFilePath);
//        String initArgs[] = {"110114","{\"name\":\"zhangsan\",\"identity\":\"110114\",\"mobile\":\"18910012222\"}"};
//        fabricClient.invoke("mychannel", TransactionRequest.Type.GO_LANG,"basicinfo",order,peers,"save",initArgs);
//    }

    //查询合约
//   public static void main(String[] args) throws InvalidKeySpecException, NoSuchAlgorithmException, CryptoException, IOException, IllegalAccessException, InvalidArgumentException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, org.hyperledger.fabric.sdk.exception.CryptoException, ProposalException, TransactionException, org.bouncycastle.crypto.CryptoException {
//        UserContext userContext = new UserContext();
//        userContext.setAffiliation("Org1");
//        userContext.setMspId("Org1MSP");
//        userContext.setAccount("李伟");
//        userContext.setName("admin");
//        Enrollment enrollment =  UserUtils.getEnrollment(keyFolderPath1,keyFileName1,certFoldePath1,certFileName1);
//        userContext.setEnrollment(enrollment);
//        FabricClient fabricClient = new FabricClient(userContext);
//        Peer peer0 = fabricClient.getPeer("peer0.org1.example.com","grpcs://peer0.org1.example.com:7051",tlsPeerFilePath);
//        Peer peer1 = fabricClient.getPeer("peer0.org2.example.com","grpcs://peer0.org2.example.com:9051",tlsPeerFilePathAddtion);
//        List<Peer> peers = new ArrayList<>();
//        peers.add(peer0);
//        peers.add(peer1);
//        String initArgs[] = {"110114"};
//        Map map =  fabricClient.queryChaincode(peers,"mychannel", TransactionRequest.Type.GO_LANG,"basicinfo","query",initArgs);
//        System.out.println(map);
//    }

   //注册用户 hqCZUStrRTAR
//   public static void main(String[] args) throws Exception {
//        FabricCAClient caClient = new FabricCAClient("http://192.168.70.43",null);
//        UserContext register = new UserContext();
//        register.setName("lihua");
//        register.setAffiliation("org2");
//        Enrollment enrollment = caClient.enroll("admin","adminpw");
//        UserContext registar = new UserContext();
//        registar.setName("admin");
//        registar.setAffiliation("org2");
//        registar.setEnrollment(enrollment);
//       String secret =  caClient.register(registar,register);
//       System.out.println(secret);
//    }

   //注册用户查询合约
//    public static void main(String[] args) throws InvalidKeySpecException, NoSuchAlgorithmException, CryptoException, IOException, IllegalAccessException, InvalidArgumentException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, CryptoException, ProposalException, TransactionException, org.bouncycastle.crypto.CryptoException, EnrollmentException, org.hyperledger.fabric_ca.sdk.exception.InvalidArgumentException {
//        FabricCAClient caClient = new FabricCAClient("http://192.168.70.43",null);
//        UserContext userContext = new UserContext();
//        userContext.setAffiliation("Org2");
//        userContext.setMspId("Org2MSP");
//        userContext.setAccount("李伟");
//        userContext.setName("admin");
//        Enrollment enrollment = caClient.enroll("lihua","hqCZUStrRTAR");
//        userContext.setEnrollment(enrollment);
//        FabricClient fabricClient = new FabricClient(userContext);
//        Peer peer0 = fabricClient.getPeer("peer0.org1.example.com","grpcs://peer0.org1.example.com:7051",tlsPeerFilePath);
//        Peer peer1 = fabricClient.getPeer("peer0.org2.example.com","grpcs://peer0.org2.example.com:9051",tlsPeerFilePathAddtion);
//        List<Peer> peers = new ArrayList<>();
//        peers.add(peer0);
//        peers.add(peer1);
//        String initArgs[] = {"110120"};
//        Map map =  fabricClient.queryChaincode(peers,"mychannel", TransactionRequest.Type.GO_LANG,"basicinfo","query",initArgs);
//        System.out.println(map);
//    }



    //注册用户invoke合约
//    public static void main(String[] args) throws InvalidKeySpecException, NoSuchAlgorithmException, CryptoException, IOException, IllegalAccessException, InvalidArgumentException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, org.hyperledger.fabric.sdk.exception.CryptoException, ProposalException, TransactionException, ChaincodeEndorsementPolicyParseException, EnrollmentException, org.hyperledger.fabric_ca.sdk.exception.InvalidArgumentException {
//        FabricCAClient caClient = new FabricCAClient("http://192.168.70.43",null);
//        UserContext userContext = new UserContext();
//        userContext.setAffiliation("Org2");
//        userContext.setMspId("Org2MSP");
//        userContext.setAccount("李伟");
//        userContext.setName("admin");
//        Enrollment enrollment = caClient.enroll("lihua","hqCZUStrRTAR");
//        userContext.setEnrollment(enrollment);
//        FabricClient fabricClient = new FabricClient(userContext);
//        Peer peer0 = fabricClient.getPeer("peer0.org1.example.com","grpcs://peer0.org1.example.com:7051",tlsPeerFilePath);
//        Peer peer1 = fabricClient.getPeer("peer0.org2.example.com","grpcs://peer0.org2.example.com:9051",tlsPeerFilePathAddtion);
//        List<Peer> peers = new ArrayList<>();
//        peers.add(peer0);
//        peers.add(peer1);
//        Orderer order = fabricClient.getOrderer("orderer.example.com","grpcs://orderer.example.com:7050",tlsOrderFilePath);
//        String initArgs[] = {"110120","{\"name\":\"zhangsan\",\"identity\":\"110120\",\"mobile\":\"18910012222\"}"};
//        fabricClient.invoke("mychannel", TransactionRequest.Type.GO_LANG,"basicinfo",order,peers,"save",initArgs);
//    }
}
