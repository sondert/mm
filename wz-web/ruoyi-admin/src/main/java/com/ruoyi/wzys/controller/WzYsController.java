package com.ruoyi.wzys.controller;

import java.util.LinkedList;
import java.util.List;

import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.sdk.po.YsHis;
import com.ruoyi.sdk.util.YsUtil;
import com.ruoyi.wzck.domain.WzCk;
import com.ruoyi.wzck.service.IWzCkService;
import com.ruoyi.wzkc.domain.WzKc;
import com.ruoyi.wzkc.service.IWzKcService;
import com.ruoyi.wzmx.domain.WzMx;
import com.ruoyi.wzmx.service.IWzMxService;
import com.ruoyi.wzwz.domain.WzWz;
import com.ruoyi.wzwz.service.IWzWzService;
import com.ruoyi.wzys.domain.KcVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wzys.domain.WzYs;
import com.ruoyi.wzys.service.IWzYsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 物资运输Controller
 * 
 * @author ruoyi
 * @date 2021-11-14
 */
@RestController
@RequestMapping("/wzys/wzys")
public class WzYsController extends BaseController
{
    @Autowired
    private IWzYsService wzYsService;

    @Autowired
    private IWzKcService wzKcService;

    @Autowired
    private YsUtil ysUtil;


    @Autowired
    private IWzCkService wzCkService;

    @Autowired
    private IWzWzService wzWzService;

    @Autowired
    private IWzMxService wzMxService;
    /**
     * 查询物资运输列表
     */
    @PreAuthorize("@ss.hasPermi('wzys:wzys:list')")
    @GetMapping("/list")
    public TableDataInfo list(WzYs wzYs)
    {
        startPage();
        List<WzYs> list = wzYsService.selectWzYsList(wzYs);
        return getDataTable(list);
    }
    @PreAuthorize("@ss.hasPermi('wzys:wzys:kc')")
    @GetMapping("/kc/{ckid}")
    public TableDataInfo kc(@PathVariable Integer ckid)
    {
        WzKc wzKc = new WzKc();
        wzKc.setCkCkid(ckid);
        List<WzKc> list = wzKcService.selectWzKcList(wzKc);
        if(list.size()==0){
            return getDataTable(new LinkedList<KcVo>());
        }
        List<KcVo> list2 = new LinkedList<KcVo>();
        for(WzKc kc:list){
            WzCk ck = wzCkService.selectWzCkById(kc.getCkCkid());
            WzWz wz = wzWzService.selectWzWzById(kc.getKcWzid());
            KcVo kcVo = new KcVo();
            kcVo.setId(kc.getId());
            kcVo.setCkCkName(ck.getCkName());
            kcVo.setKcTp(wz.getWzTp());
            kcVo.setKcWzname(wz.getWzName());
            kcVo.setKcGg(wz.getWzName());
            kcVo.setNum(kc.getNum());
            kcVo.setKcDw(wz.getWzDw());
            kcVo.setNownum(1);
            list2.add(kcVo);
        }
        return getDataTable(list2);
    }
    @PreAuthorize("@ss.hasPermi('wzys:wzys:bd')")
    @PutMapping("/bd")
    public AjaxResult bd()
    {
        List<WzYs> list = wzYsService.selectWzYsList(new WzYs());
        int rows =1;
        for (WzYs wzYs:list) {
            try {
                if(wzYs.getYsDh().equals(new Integer(4))){
                    break;
                }
                WzYs wzYs1 = ysUtil.query(wzYs.getYsDh());
                wzYs1.setId(wzYs.getId());
                if(!wzYs1.equals(wzYs)){
                    rows=0;
                    wzYsService.updateWzYs(wzYs1);
                }
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("通过id查询区块链运输信息失败");
            }
        }
        return rows>0?AjaxResult.success("比对成功，数据一致"):AjaxResult.error("数据不一致，更新成功");
    }

    /**
     * 导出物资运输列表
     */
    @PreAuthorize("@ss.hasPermi('wzys:wzys:export')")
    @Log(title = "物资运输", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WzYs wzYs)
    {
        List<WzYs> list = wzYsService.selectWzYsList(wzYs);
        ExcelUtil<WzYs> util = new ExcelUtil<WzYs>(WzYs.class);
        return util.exportExcel(list, "物资运输数据");
    }

    /**
     * 获取物资运输详细信息
     */
    @PreAuthorize("@ss.hasPermi('wzys:wzys:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(wzYsService.selectWzYsById(id));
    }

    /**
     * 新增物资运输
     */
    @PreAuthorize("@ss.hasPermi('wzys:wzys:add')")
    @Log(title = "物资运输", businessType = BusinessType.INSERT)
    @PostMapping(value = "/{ids}/{nums}")
    public AjaxResult add(@RequestBody WzYs wzYs,@PathVariable Integer[] ids,@PathVariable Integer[] nums)
    {
        String dh = IdUtils.fastSimpleUUID();
        wzYs.setYsDh(dh);
        wzYs.setYsZt(1);
        int rows = wzYsService.insertWzYs(wzYs);
        if(rows>0){
            try {
                ysUtil.save(wzYs.getYsDh(),wzYs);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("物资运输区块链添加信息异常");
            }
        }else {
            System.out.println("物资运输区块链添加信息失败");
        }
        if(rows>0){
            int i=0;
            while(i<ids.length){
                WzKc wzKc = wzKcService.selectWzKcById(ids[i]);
                WzWz wzWz = wzWzService.selectWzWzById(wzKc.getKcWzid());
                WzMx wzMx = new WzMx();
                wzMx.setMxDh(dh);
                wzMx.setMxMc(wzWz.getWzName());
                wzMx.setMxSpid(wzWz.getId());
                wzMx.setMxGg(wzWz.getWzGg());
                wzMx.setMxTp(wzWz.getWzTp());
                wzMx.setMxNum(nums[i]);
                wzMx.setMxDw(wzWz.getWzDw());
                wzMx.setMxType(2);
                int row2 = wzMxService.insertWzMx(wzMx);
                if(row2<=0){
                    rows=0;
                }
//                WzKc kc = new WzKc();
//                kc.setCkCkid(wzYs.getYsCh());
//                kc.setKcWzid(wzWz.getId());
//                int row3=1;
//                wzKc.setNum(wzKc.getNum()-nums[i]);
//                row3 = wzKcService.updateWzKc(wzKc);
//                if(row3<=0){
//                    rows=0;
//                }
                i++;
            }
        }
        return toAjax(rows);

    }

    /**
     * 修改物资运输
     */
    @PreAuthorize("@ss.hasPermi('wzys:wzys:edit')")
    @Log(title = "物资运输", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WzYs wzYs)
    {
        int rows = wzYsService.updateWzYs(wzYs);
        if(rows>0){
            try {
                ysUtil.save(wzYs.getYsDh(),wzYs);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("修改物资运输信息时遇到异常");
            }
        }else {
            System.out.println("修改物资运输信息时失败");
        }
        return toAjax(rows);
    }

    /**
     * 删除物资运输
     */
//    @PreAuthorize("@ss.hasPermi('wzys:wzys:remove')")
//    @Log(title = "物资运输", businessType = BusinessType.DELETE)
//	@DeleteMapping("/{ids}")
//    public AjaxResult remove(@PathVariable Integer[] ids)
//    {
//        for (int id:ids) {
//            WzYs wzYs = wzYsService.selectWzYsById(id);
//            try {
//                ysUtil.delete(wzYs.getYsDh());
//            } catch (Exception e) {
//                e.printStackTrace();
//                System.out.println("批量删除物资运输信息时间遇到异常");
//            }
//        }
//        return toAjax(wzYsService.deleteWzYsByIds(ids));
//    }
    @PreAuthorize("@ss.hasPermi('wzys:wzys:remove')")
    @Log(title = "物资运输", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        int rows = 1;
        for (int id:ids) {
            WzYs wzYs = wzYsService.selectWzYsById(id);
            wzYs.setYsZt(4);
            int row = wzYsService.updateWzYs(wzYs);
            if(row<=0){
                rows=0;
            }
            if(rows>0){
                try {
                    ysUtil.delete(wzYs.getYsDh());
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("修改物资运输区块链信息时遇到异常");
                }
            }else {
                System.out.println("修改物资运输信息时失败");
            }
        }
        return toAjax(rows);
    }

    /**
     * 溯源
     */
    @PreAuthorize("@ss.hasPermi('wzys:wzys:trace')")
    @GetMapping(value = "/trace/{id}")
    public TableDataInfo trace(@PathVariable("id") Integer id)
    {
        List<YsHis> list = new LinkedList<>();
        try {
            YsHis[] ysHis = ysUtil.queryAll(wzYsService.selectWzYsById(id).getYsDh());

            for (YsHis his:ysHis) {
                if(Boolean.parseBoolean(his.IsDelete)){
                    WzYs wzys = (WzYs) list.get(list.size()-1).THistory.clone();
                    wzys.setYsZt(4);
                    his.setTHistory(wzys);
                }
                list.add(his);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("查询物资运输历史信息失败");
        }
        return getDataTable(list);
    }

    /**
     * 运输
     */
    @PreAuthorize("@ss.hasPermi('wzys:wzys:ys')")
    @Log(title = "物资运输表", businessType = BusinessType.UPDATE)
    @PutMapping(value = "/ys/{id}")
    public AjaxResult ys(@PathVariable("id") Integer id)
    {
        WzYs wzys = wzYsService.selectWzYsById(id);
        wzys.setYsZt(2);
        int rows = wzYsService.updateWzYs(wzys);
        if(rows>0){
            try {
                ysUtil.save(wzys.getYsDh(),wzys);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("修改物资运输区块链信息时遇到异常");
            }
        }else {
            System.out.println("修改物资运输信息时失败");
        }
        if(rows>0){
            WzMx wzMx = new WzMx();
            wzMx.setMxDh(wzys.getYsDh());
            List<WzMx> list = wzMxService.selectWzMxList(wzMx);
            for (WzMx mx:list) {
                WzKc wzKc = new WzKc();
                wzKc.setCkCkid(wzys.getYsCh());
                wzKc.setKcWzid(mx.getMxSpid());
                List<WzKc> kcs= wzKcService.selectWzKcList(wzKc);
                if(kcs.size()==1){
                    wzKc = kcs.get(0);
                    wzKc.setNum(wzKc.getNum()-mx.getMxNum());
                    int row = wzKcService.updateWzKc(wzKc);
                    if(row<=0){
                        rows=0;
                    }
                }else {
                    rows=0;
                }
            }
        }

        return toAjax(rows);
    }
    /**
     * 送达
     */
    @PreAuthorize("@ss.hasPermi('wzys:wzys:ys')")
    @Log(title = "物资运输表", businessType = BusinessType.UPDATE)
    @PutMapping(value = "/sd/{id}")
    public AjaxResult sd(@PathVariable("id") Integer id)
    {
        WzYs wzys = wzYsService.selectWzYsById(id);
        wzys.setYsZt(3);
        int rows = wzYsService.updateWzYs(wzys);
        if(rows>0){
            try {
                ysUtil.save(wzys.getYsDh(),wzys);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("修改物资运输区块链信息时遇到异常");
            }
        }else {
            System.out.println("修改物资运输信息时失败");
        }
        if(rows>0){
            WzMx wzMx = new WzMx();
            wzMx.setMxDh(wzys.getYsDh());
            List<WzMx> list = wzMxService.selectWzMxList(wzMx);
            for (WzMx mx:list) {
                WzKc wzKc = new WzKc();
                wzKc.setCkCkid(wzys.getYsRh());
                wzKc.setKcWzid(mx.getMxSpid());
                List<WzKc> kcs= wzKcService.selectWzKcList(wzKc);
                if(kcs.size()==1){
                    wzKc = kcs.get(0);
                    wzKc.setNum(wzKc.getNum()+mx.getMxNum());
                    int row = wzKcService.updateWzKc(wzKc);
                    if(row<=0){
                        rows=0;
                    }
                }else {
                    rows=0;
                }
            }
        }
        return toAjax(rows);
    }

    @PreAuthorize("@ss.hasPermi('wzys:wzys:query')")
    @GetMapping(value = "/mx/{id}")
    public TableDataInfo mx(@PathVariable("id") Integer id)
    {
        WzMx wzMx = new WzMx();
        wzMx.setMxDh(wzYsService.selectWzYsById(id).getYsDh());
        List<WzMx> list = wzMxService.selectWzMxList(wzMx);
        List<WzMx> ans = new LinkedList<>();
        for (WzMx mx:list) {
            if(mx.getMxType()==2){
                ans.add(mx);
            }
        }
        return getDataTable(ans);
    }

    /**
     * 修改物资运输位置
     */
    @PreAuthorize("@ss.hasPermi('wzys:wzys:wz')")
    @Log(title = "物资运输", businessType = BusinessType.UPDATE)
    @PostMapping("/wz/{id}/{wz}")
    public AjaxResult wz(@PathVariable Integer id,@PathVariable String wz)
    {
        WzYs wzYs = wzYsService.selectWzYsById(id);
        wzYs.setYsWz(wz);
        int rows = wzYsService.updateWzYs(wzYs);
        if(rows>0){
            try {
                ysUtil.save(wzYs.getYsDh(),wzYs);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("修改物资运输信息时遇到异常");
            }
        }else {
            System.out.println("修改物资运输信息时失败");
        }
        return toAjax(rows);
    }
}
