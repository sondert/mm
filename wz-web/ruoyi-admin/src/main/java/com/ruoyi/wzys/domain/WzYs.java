package com.ruoyi.wzys.domain;

import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 物资运输对象 wz_ys
 * 
 * @author ruoyi
 * @date 2021-11-14
 */
public class WzYs extends BaseEntity implements Cloneable
{
    private static final long serialVersionUID = 1L;

    /** 运输id */
    private Integer id;

    /** 运输单号 */
    @Excel(name = "运输单号")
    private String ysDh;

    /** 运输路线 */
    @Excel(name = "运输路线")
    private String ysLx;

    /** 当前位置 */
    @Excel(name = "当前位置")
    private String ysWz;

    /** 负责人 */
    @Excel(name = "负责人")
    private String ysFzr;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String ysLxfs;

    /** 运输车辆 */
    @Excel(name = "运输车辆")
    private Integer ysCl;

    /** 出货仓库 */
    @Excel(name = "出货仓库")
    private Integer ysCh;

    /** 入货仓库 */
    @Excel(name = "入货仓库")
    private Integer ysRh;

    /** 起送时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "起送时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date ysQtime;

    /** 到达时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到达时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date ysDtime;

    /** 状态 */
    @Excel(name = "状态")
    private int ysZt;

    /** 备注 */
    @Excel(name = "备注")
    private String ysBz;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setYsDh(String ysDh) 
    {
        this.ysDh = ysDh;
    }

    public String getYsDh() 
    {
        return ysDh;
    }
    public void setYsLx(String ysLx) 
    {
        this.ysLx = ysLx;
    }

    public String getYsLx() 
    {
        return ysLx;
    }
    public void setYsWz(String ysWz) 
    {
        this.ysWz = ysWz;
    }

    public String getYsWz() 
    {
        return ysWz;
    }
    public void setYsFzr(String ysFzr) 
    {
        this.ysFzr = ysFzr;
    }

    public String getYsFzr() 
    {
        return ysFzr;
    }
    public void setYsLxfs(String ysLxfs) 
    {
        this.ysLxfs = ysLxfs;
    }

    public String getYsLxfs() 
    {
        return ysLxfs;
    }
    public void setYsCl(Integer ysCl) 
    {
        this.ysCl = ysCl;
    }

    public Integer getYsCl() 
    {
        return ysCl;
    }
    public void setYsCh(Integer ysCh) 
    {
        this.ysCh = ysCh;
    }

    public Integer getYsCh() 
    {
        return ysCh;
    }
    public void setYsRh(Integer ysRh) 
    {
        this.ysRh = ysRh;
    }

    public Integer getYsRh() 
    {
        return ysRh;
    }
    public void setYsQtime(Date ysQtime) 
    {
        this.ysQtime = ysQtime;
    }

    public Date getYsQtime() 
    {
        return ysQtime;
    }
    public void setYsDtime(Date ysDtime) 
    {
        this.ysDtime = ysDtime;
    }

    public Date getYsDtime() 
    {
        return ysDtime;
    }
    public void setYsZt(int ysZt)
    {
        this.ysZt = ysZt;
    }

    public int getYsZt()
    {
        return ysZt;
    }
    public void setYsBz(String ysBz) 
    {
        this.ysBz = ysBz;
    }

    public String getYsBz() 
    {
        return ysBz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WzYs wzYs = (WzYs) o;
        return ysZt == wzYs.ysZt && Objects.equals(id, wzYs.id) && Objects.equals(ysDh, wzYs.ysDh) && Objects.equals(ysLx, wzYs.ysLx) && Objects.equals(ysWz, wzYs.ysWz) && Objects.equals(ysFzr, wzYs.ysFzr) && Objects.equals(ysLxfs, wzYs.ysLxfs) && Objects.equals(ysCl, wzYs.ysCl) && Objects.equals(ysCh, wzYs.ysCh) && Objects.equals(ysRh, wzYs.ysRh) && Objects.equals(ysBz, wzYs.ysBz);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ysDh, ysLx, ysWz, ysFzr, ysLxfs, ysCl, ysCh, ysRh, ysZt, ysBz);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("ysDh", getYsDh())
            .append("ysLx", getYsLx())
            .append("ysWz", getYsWz())
            .append("ysFzr", getYsFzr())
            .append("ysLxfs", getYsLxfs())
            .append("ysCl", getYsCl())
            .append("ysCh", getYsCh())
            .append("ysRh", getYsRh())
            .append("ysQtime", getYsQtime())
            .append("ysDtime", getYsDtime())
            .append("ysZt", getYsZt())
            .append("ysBz", getYsBz())
            .toString();
    }
}
