package com.ruoyi.wzys.domain;

import com.ruoyi.common.annotation.Excel;
import io.swagger.models.auth.In;

/**
 * @Description: TODO
 * @author: sonder
 * @date: 2021年12月05日 19:10
 */
public class KcVo {
    private Integer id;
    private String ckCkName;
    private String  kcTp;
    private String kcWzname;
    private String  kcGg;
    private Integer num;
    private String  kcDw;
    private Integer nownum;

    public Integer getNownum() {
        return nownum;
    }

    public void setNownum(Integer nownum) {
        this.nownum = nownum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCkCkName() {
        return ckCkName;
    }

    public void setCkCkName(String ckCkName) {
        this.ckCkName = ckCkName;
    }

    public String getKcTp() {
        return kcTp;
    }

    public void setKcTp(String kcTp) {
        this.kcTp = kcTp;
    }

    public String getKcWzname() {
        return kcWzname;
    }

    public void setKcWzname(String kcWzname) {
        this.kcWzname = kcWzname;
    }

    public String getKcGg() {
        return kcGg;
    }

    public void setKcGg(String kcGg) {
        this.kcGg = kcGg;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getKcDw() {
        return kcDw;
    }

    public void setKcDw(String kcDw) {
        this.kcDw = kcDw;
    }
}
