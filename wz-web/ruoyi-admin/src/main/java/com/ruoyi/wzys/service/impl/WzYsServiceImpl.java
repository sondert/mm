package com.ruoyi.wzys.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wzys.mapper.WzYsMapper;
import com.ruoyi.wzys.domain.WzYs;
import com.ruoyi.wzys.service.IWzYsService;

/**
 * 物资运输Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-11-14
 */
@Service
public class WzYsServiceImpl implements IWzYsService 
{
    @Autowired
    private WzYsMapper wzYsMapper;

    /**
     * 查询物资运输
     * 
     * @param id 物资运输主键
     * @return 物资运输
     */
    @Override
    public WzYs selectWzYsById(Integer id)
    {
        return wzYsMapper.selectWzYsById(id);
    }

    /**
     * 查询物资运输列表
     * 
     * @param wzYs 物资运输
     * @return 物资运输
     */
    @Override
    public List<WzYs> selectWzYsList(WzYs wzYs)
    {
        return wzYsMapper.selectWzYsList(wzYs);
    }

    /**
     * 新增物资运输
     * 
     * @param wzYs 物资运输
     * @return 结果
     */
    @Override
    public int insertWzYs(WzYs wzYs)
    {
        return wzYsMapper.insertWzYs(wzYs);
    }

    /**
     * 修改物资运输
     * 
     * @param wzYs 物资运输
     * @return 结果
     */
    @Override
    public int updateWzYs(WzYs wzYs)
    {
        return wzYsMapper.updateWzYs(wzYs);
    }

    /**
     * 批量删除物资运输
     * 
     * @param ids 需要删除的物资运输主键
     * @return 结果
     */
    @Override
    public int deleteWzYsByIds(Integer[] ids)
    {
        return wzYsMapper.deleteWzYsByIds(ids);
    }

    /**
     * 删除物资运输信息
     * 
     * @param id 物资运输主键
     * @return 结果
     */
    @Override
    public int deleteWzYsById(Integer id)
    {
        return wzYsMapper.deleteWzYsById(id);
    }
}
