package com.ruoyi.wzys.mapper;

import java.util.List;
import com.ruoyi.wzys.domain.WzYs;

/**
 * 物资运输Mapper接口
 * 
 * @author ruoyi
 * @date 2021-11-14
 */
public interface WzYsMapper 
{
    /**
     * 查询物资运输
     * 
     * @param id 物资运输主键
     * @return 物资运输
     */
    public WzYs selectWzYsById(Integer id);

    /**
     * 查询物资运输列表
     * 
     * @param wzYs 物资运输
     * @return 物资运输集合
     */
    public List<WzYs> selectWzYsList(WzYs wzYs);

    /**
     * 新增物资运输
     * 
     * @param wzYs 物资运输
     * @return 结果
     */
    public int insertWzYs(WzYs wzYs);

    /**
     * 修改物资运输
     * 
     * @param wzYs 物资运输
     * @return 结果
     */
    public int updateWzYs(WzYs wzYs);

    /**
     * 删除物资运输
     * 
     * @param id 物资运输主键
     * @return 结果
     */
    public int deleteWzYsById(Integer id);

    /**
     * 批量删除物资运输
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWzYsByIds(Integer[] ids);
}
